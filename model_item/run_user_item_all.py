#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from model_item.run_user_item import *

if __name__ == '__main__':
    train_frame, test_frames = run(sys.argv[1], sys.argv[2])
    run_by_feature_file(train_frame, test_frames, '../model_item/feature.txt', sys.argv[3], sys.argv[1],
                        sys.argv[2])
