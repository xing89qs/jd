#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from util import *
from util import npz_reader as npz
import numpy as np
import pandas as pd
import xgboost as xgb
from xgboost import XGBClassifier, XGBRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression


def dfs(l, r, folder, files):
    if l == r:
        # temp_frame = pd.read_csv('../feature_pool/' + folder + '/' + files[l] + '.csv')
        temp_frame = npz.load_npz('../feature_pool/' + folder + '/' + files[l] + '.npz')
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, folder, files)
    r_frame = dfs(mid + 1, r, folder, files)
    ret = pd.merge(l_frame, r_frame, how='left')
    del l_frame
    del r_frame
    return ret


def create_dataset(folder, files):
    with open('../feature_pool/' + folder + '/buy_cate.txt') as f:
        l = [int(str(x).strip()) for x in f][0]
    data_set = dfs(0, len(files) - 1, folder, files)
    # data_set.to_csv('../feature_pool/' + folder + '/data.csv', index=False)
    return data_set, l


def __f(x):
    return x.tolist()[0]


def calculate(frame, buy_user, test_user_predict_frame, result_folder=None):
    frame = frame[frame.cate == 8]
    frame.sort_values(by='pred', inplace=True)
    frame_sku = frame.groupby(['user_id'], as_index=False)['sku_id'].agg(lambda x: x.tolist()[0])
    frame_sku = pd.merge(frame_sku, frame[['user_id', 'sku_id', 'y', 'pred']], how='left')

    user_frame = frame.groupby(['user_id'], as_index=False)['pred'].agg(np.min)
    user_frame.to_csv(result_folder + '/result_all_user.csv')

    for use_candidate in [600, 650, 700, 750, 800]:
        try:
            now_frame = test_user_predict_frame[0:use_candidate]
            now_frame = pd.merge(now_frame, frame_sku, how='left')
            pred_user = len(now_frame)
            correct_frame = now_frame[now_frame.y_user == 1]
            correct_user = len(correct_frame)
            print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
            precision = correct_user / pred_user
            recall = correct_user / buy_user
            F1_1 = 6 * precision * recall / (5 * recall + precision)

            correct_sku = len(now_frame[now_frame.y == 1])
            print("correct_sku = %d, pred_user = %d, buy_user = %d" % (correct_sku, pred_user, buy_user))
            precision = correct_sku / pred_user
            recall = correct_sku / buy_user
            F1_2 = 5 * precision * recall / (2 * recall + 3 * precision)

            print("F1_1 = %f, F1_2 = %f, score = %f" % (F1_1, F1_2, F1_1 * 0.4 + F1_2 * 0.6))
            print("--------------------------------------------------------------------------------------------------")
        except ZeroDivisionError as e:
            print('Error! No prediction match!')
            pass
        if result_folder is not None:
            if not os.path.exists(result_folder):
                os.mkdir(result_folder)
            now_frame[['user_id', 'sku_id']].to_csv(result_folder + '/result_' + str(use_candidate) + '.csv',
                                                    index=False)

        pass

    for extra in [100, 200]:
        extra_predict = frame[0:extra][['user_id', 'sku_id', 'y']]
        extra_predict['y_user'] = extra_predict['y']

        for use_candidate in [600, 650, 700, 750, 800]:
            try:
                now_frame = test_user_predict_frame[0:use_candidate]
                now_frame = pd.merge(now_frame, frame_sku, how='left')
                now_frame = pd.concat(
                    [now_frame[['user_id', 'sku_id', 'y_user']], extra_predict[['user_id', 'sku_id', 'y_user']]])
                now_frame = now_frame.drop_duplicates()
                pred_user = len(now_frame)
                correct_frame = now_frame[now_frame.y_user == 1]
                correct_user = len(correct_frame)
                print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
                precision = correct_user / pred_user
                recall = correct_user / buy_user
                F1_1 = 6 * precision * recall / (5 * recall + precision)

                now_frame = pd.merge(now_frame, frame_sku[['user_id', 'sku_id', 'y']], how='left')
                correct_sku = len(now_frame[now_frame.y == 1])
                print("correct_sku = %d, pred_user = %d, buy_user = %d" % (correct_sku, pred_user, buy_user))
                precision = correct_sku / pred_user
                recall = correct_sku / buy_user
                F1_2 = 5 * precision * recall / (2 * recall + 3 * precision)

                print("F1_1 = %f, F1_2 = %f, score = %f" % (F1_1, F1_2, F1_1 * 0.4 + F1_2 * 0.6))
                print(
                    "--------------------------------------------------------------------------------------------------")
            except ZeroDivisionError as e:
                print('Error! No prediction match!')
                pass
            if result_folder is not None:
                if not os.path.exists(result_folder):
                    os.mkdir(result_folder)
                now_frame[['user_id', 'sku_id']].to_csv(
                    result_folder + '/result_with_item_' + str(use_candidate) + '.csv',
                    index=False)

            pass

    print('------Only User----------------')
    frame_sku.sort_values(by='pred', inplace=True)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800]:
        try:
            now_frame = frame_sku[0:use_candidate]
            pred_user = len(now_frame)
            correct_frame = now_frame[now_frame.y_user == 1]
            correct_user = len(correct_frame)
            print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
            precision = correct_user / pred_user
            recall = correct_user / buy_user
            F1_1 = 6 * precision * recall / (5 * recall + precision)

            correct_sku = len(now_frame[now_frame.y == 1])
            print("correct_sku = %d, pred_user = %d, buy_user = %d" % (correct_sku, pred_user, buy_user))
            precision = correct_sku / pred_user
            recall = correct_sku / buy_user
            F1_2 = 5 * precision * recall / (2 * recall + 3 * precision)

            print("F1_1 = %f, F1_2 = %f, score = %f" % (F1_1, F1_2, F1_1 * 0.4 + F1_2 * 0.6))
            print("--------------------------------------------------------------------------------------------------")
        except ZeroDivisionError as e:
            print('Error! No prediction match!')
            pass

        pass


def weight(x, only_8):
    arg1 = 5 if x[0] == 8 else 1
    arg2 = 50 if x[1] == 1 else 1
    if only_8:
        arg1 = 1
    return arg1 * arg2


def run_by_feature_file(train_frame, test_frames, feature_file, result_file, train_set, test_set,
                        model_name='xgb', label='y', only_8=False):
    features = read_feature(feature_file)
    if model_name == 'xgb':
        model = XGBClassifier(n_estimators=500, max_depth=3, learning_rate=0.05, objective='binary:logistic',
                              subsample=0.8,
                              colsample_bytree=0.8)
    else:
        model = RandomForestClassifier(n_estimators=1000, max_depth=6, max_features=0.3, criterion='gini', n_jobs=-1)
    model.fit(train_frame[features].as_matrix(), train_frame[label].as_matrix(),
              sample_weight=[weight(x, only_8) for x in train_frame[['cate', label]].as_matrix()])

    try:
        if model_name == 'xgb':
            mapFeat = dict(zip(["f" + str(i) for i in range(len(features))], features))
            ts = pd.Series(model.booster().get_fscore())
            ts.index = ts.reset_index()['index'].map(mapFeat)
            with open('best_features' + str(train_set) + '.feature', 'w') as file:
                for f in ts.order().index[-300:]:
                    file.write(str(f) + ',' + str(ts[f]) + '\n')
    except:
        pass

    test_user_predict_frames = [pd.read_csv('../feature_pool/' + file + '/result_user.csv') for file in
                                str(test_set).split(',')]
    i = 0

    for test_frame, test_frame_buy_user in test_frames:
        test_frame.fillna(-1, downcast='infer', inplace=True)
        test_frame['pred'] = [x[0] for x in model.predict_proba(test_frame[features].as_matrix())]
        calculate(test_frame, test_frame_buy_user, test_user_predict_frames[i], result_file)
        i += 1
    pass


def run(train_set, test_set):
    feature_files = ['data']
    train_frame = pd.concat([create_dataset(data_set, feature_files)[0] for data_set in str(train_set).split(',')])
    test_frames = [create_dataset(data_set, feature_files) for data_set in str(test_set).split(',')]

    train_frame.fillna(-1, downcast='infer', inplace=True)
    for test_frame, test_frame_buy_user in test_frames:
        test_frame.fillna(-1, downcast='infer', inplace=True)
    return train_frame, test_frames
    pass


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2])
