#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from util import *
import numpy as np
import pandas as pd
import xgboost as xgb
from xgboost import XGBClassifier, XGBRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression


def dfs(l, r, folder, files):
    if l == r:
        temp_frame = pd.read_csv('../feature_pool/' + folder + '/' + files[l] + '.csv')
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, folder, files)
    r_frame = dfs(mid + 1, r, folder, files)
    ret = pd.merge(l_frame, r_frame, how='left')
    del l_frame
    del r_frame
    return ret


def create_dataset(folder, files):
    with open('../feature_pool/' + folder + '/buy_cate.txt') as f:
        l = [int(str(x).strip()) for x in f][0]
    data_set = dfs(0, len(files) - 1, folder, files)
    data_set.to_csv('../feature_pool/' + folder + '/data.csv', index=False)
    return data_set, l


def __f(x):
    return x.tolist()[0]


def calculate(frame, buy_user, result_file=None):
    frame = frame[frame.cate == 8]
    frame.sort_values(by='pred', inplace=True)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800, 900, 1000, 1100, 1200]:
        try:
            now_frame = frame[0:use_candidate]
            pred_user = len(now_frame)
            correct_frame = now_frame[now_frame.y == 1]
            correct_user = len(correct_frame)
            print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
            precision = correct_user / pred_user
            recall = correct_user / buy_user
            F1_1 = 6 * precision * recall / (5 * recall + precision)
            print("F1_1 = %f, " % (F1_1))
            print("--------------------------------------------------------------------------------------------------")
        except ZeroDivisionError as e:
            print('Error! No prediction match!')
            pass
        if use_candidate == 700 and result_file is not None:
            now_frame['sku_id'] = 56792
            now_frame[['user_id', 'sku_id']].to_csv(result_file, index=False)

        pass


def run_model(train_frame, test_frames, feature_file, train_set, result_file=None):
    features = read_feature(feature_file)
    params = {'objective': 'binary:logistic', 'eval_metric': 'logloss', 'eta': 0.05, 'max_depth': 3, 'silent': 1,
              'colsample_bytree': 0.8, 'subsample': 0.8}
    train_x = train_frame[features].as_matrix()
    train_y = train_frame['y'].as_matrix()
    train_matrix = xgb.DMatrix(train_x, label=train_y, weight=[x for x in map(lambda x: 50 if x == 1 else 1, train_y)])
    watch_list = []
    watch_list.append((train_matrix, 'train'))
    i = 0
    for test_frame, test_frame_buy_user in test_frames:
        test_x = test_frame[features].as_matrix()
        test_y = test_frame['y'].as_matrix()
        test_matrix = xgb.DMatrix(test_x, label=test_y, weight=[x for x in map(lambda x: 50 if x == 1 else 1, test_y)])
        watch_list.append((test_matrix, 'test' + str(i)))
        i += 1
    model = xgb.train(params, train_matrix, 500, watch_list, verbose_eval=10)

    mapFeat = dict(zip(["f" + str(i) for i in range(len(features))], features))
    ts = pd.Series(model.get_fscore())
    ts.index = ts.reset_index()['index'].map(mapFeat)
    with open('best_features' + str(train_set) + '.feature', 'w') as file:
        for f in ts.order().index[-300:]:
            file.write(str(f) + ',' + str(ts[f]) + '\n')

    for test_frame, test_frame_buy_user in test_frames:
        test_x = test_frame[features].as_matrix()
        test_y = test_frame['y'].as_matrix()
        test_matrix = xgb.DMatrix(test_x, label=test_y, weight=[x for x in map(lambda x: 50 if x == 1 else 1, test_y)])
        test_frame['pred'] = model.predict(test_matrix)
        calculate(test_frame, test_frame_buy_user, result_file)


def run(train_set, test_set, result_file):
    # feature_files = ['v1', 'v2', 'v3', 'v4', 'v5', 'v8', 'v9', 'v10', 'v11', 'v12', 'v13', 'v14', 'v15', 'v18','v19','v20','v22','y']
    feature_files = ['data']
    train_frame = pd.concat([create_dataset(data_set, feature_files)[0] for data_set in str(train_set).split(',')])
    test_frames = [create_dataset(data_set, feature_files) for data_set in str(test_set).split(',')]

    train_frame.fillna(-1, downcast='infer', inplace=True)
    for test_frame, test_frame_buy_user in test_frames:
        test_frame.fillna(-1, downcast='infer', inplace=True)
    return train_frame, test_frames


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2], None if len(sys.argv) <= 3 else sys.argv[3])
