#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from util import *
from util import npz_reader as npz
import numpy as np
import pandas as pd
import xgboost as xgb
from xgboost import XGBClassifier, XGBRegressor
import model.filter as filter
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from functools import reduce


def dfs(l, r, folder, files):
    if l == r:
        # temp_frame = pd.read_csv('../feature_pool/' + folder + '/' + files[l] + '.csv')
        temp_frame = npz.load_npz('../feature_pool/' + folder + '/' + files[l] + '.npz')
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, folder, files)
    r_frame = dfs(mid + 1, r, folder, files)
    ret = pd.merge(l_frame, r_frame, how='left')
    del l_frame
    del r_frame
    return ret


def create_dataset(folder, files, features=None):
    with open('../feature_pool/' + folder + '/buy_cate.txt') as f:
        l = [int(str(x).strip()) for x in f][0]
    data_set = dfs(0, len(files) - 1, folder, files)
    if features is not None:
        data_set1 = data_set[features + ['user_id', 'cate', 'y']]
        del data_set
        return data_set1, l
    else:
        return data_set, l


def __f(x):
    return x.tolist()[0]


def calculate(frame, buy_user, result_file=None):
    frame = frame[frame.cate == 8]
    frame.sort_values(by='pred', inplace=True)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800, 900, 1000, 1100, 1200]:
        try:
            now_frame = frame[0:use_candidate]
            pred_user = len(now_frame)
            correct_frame = now_frame[now_frame.y == 1]
            correct_user = len(correct_frame)
            print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
            precision = correct_user / pred_user
            recall = correct_user / buy_user
            F1_1 = 6 * precision * recall / (5 * recall + precision)
            print("F1_1 = %f, " % (F1_1))
            print("--------------------------------------------------------------------------------------------------")
        except ZeroDivisionError as e:
            print('Error! No prediction match!')
            pass
        if use_candidate == 700 and result_file is not None:
            now_frame['sku_id'] = 56792
            now_frame[['user_id', 'sku_id']].to_csv(result_file, index=False)

        pass


def weight(x):
    arg1 = 5 if x[0] == 8 else 1
    arg2 = 50 if x[1] == 1 else 1
    return arg1 * arg2


def run(train_set, test_set, result_file):
    feature_files = ['data1', 'y']
    features = read_feature('feature1.txt')
    train_frames = [create_dataset(data_set, feature_files, features)[0] for data_set in str(train_set).split(',')]
    # print(train_frame.columns)
    test_frames = [create_dataset(data_set, feature_files, features) for data_set in str(test_set).split(',')]
    print(len(train_frames[0]), len(test_frames[0][0]))

    for train_frame in train_frames:
        train_frame.fillna(-1, downcast='infer', inplace=True)
    for test_frame, test_frame_buy_user in test_frames:
        test_frame.fillna(-1, downcast='infer', inplace=True)
    # features = [x for x in filter(lambda x: True if str(x).startswith('_')else False, train_frame.columns)]

    l = len(train_frames)

    for i in range(l):
        train_frame = train_frames[i]
        model = XGBClassifier(n_estimators=500, max_depth=3, learning_rate=0.05, objective='binary:logistic',
                              subsample=0.8,
                              colsample_bytree=0.8)
        model = GradientBoostingClassifier(n_estimators=500, max_depth=3, learning_rate=0.05, subsample=0.8,
                                           max_features=0.8, random_state=0)
        model.fit(train_frame[features].as_matrix(), train_frame['y'].as_matrix(),
                  sample_weight=[weight(x) for x in train_frame[['cate', 'y']].as_matrix()])

        mapFeat = dict(zip(["f" + str(i) for i in range(len(features))], features))
        ts = pd.Series(model.booster().get_fscore())
        ts.index = ts.reset_index()['index'].map(mapFeat)
        with open('best_features' + str(train_set) + '.feature', 'w') as file:
            for f in ts.order().index[-300:]:
                file.write(str(f) + ',' + str(ts[f]) + '\n')

        for test_frame, test_frame_buy_user in test_frames:
            test_frame['pred'] = [x[0] for x in model.predict_proba(test_frame[features].as_matrix())]
            test_frame.sort_values(by='pred', inplace=True)
            test_frame['_pred' + str(i)] = range(len(test_frame))
            # calculate(test_frame, test_frame_buy_user, result_file)

    for test_frame, test_frame_buy_user in test_frames:
        test_frame['pred'] = test_frame.apply(lambda x: sum([x['_pred' + str(i)] for i in range(l)]), axis=1)
        calculate(test_frame, test_frame_buy_user, result_file)
    pass


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2], None if len(sys.argv) <= 3 else sys.argv[3])
