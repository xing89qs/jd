#!/usr/bin/env python
# -- coding:utf-8 --

import pandas as pd
import numpy as np

frame = pd.read_csv('../data/test1.csv')
frame_product = pd.read_csv('../data/JData_Product.csv')
frame_user = pd.read_csv('../data/JData_User.csv', encoding='gbk')
frame_user['is_user'] = 1
frame_product['is_product'] = 1

frame = pd.merge(frame, frame_user[['user_id', 'is_user']], how='left')
frame.fillna(-1, downcast='infer', inplace=True)
frame = pd.merge(frame, frame_product[['sku_id', 'is_product']], how='left')
frame.fillna(-1, downcast='infer', inplace=True)
print(frame[frame.is_user == -1])

result = frame[(frame.pred == 1) ]

result = result.groupby(['user_id'], as_index=False)['sku_id'].agg(np.max)
result['sku_id'] = 58568
print(len(result))

print(result['user_id'])

result.to_csv('submit21.csv', index=False)
