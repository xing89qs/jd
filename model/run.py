#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
import numpy as np
import pandas as pd
import xgboost as xgb
from xgboost import XGBClassifier, XGBRegressor
from sklearn.ensemble import RandomForestClassifier
from util import *


def dfs(l, r, folder, files):
    if l == r:
        temp_frame = pd.read_csv('../feature_pool/' + folder + '/' + files[l] + '.csv')
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, folder, files)
    r_frame = dfs(mid + 1, r, folder, files)
    return pd.merge(l_frame, r_frame, how='left')


def create_dataset(folder, files):
    with open('../feature_pool/' + folder + '/buy_user.txt') as f:
        l = [int(str(x).strip()) for x in f][0]
    return dfs(0, len(files) - 1, folder, files), l


def __f(x):
    return x.tolist()[0]


def calculate(frame, buy_user):
    subset = pd.read_csv('../data/JData_Product.csv')
    subset['is_product'] = 1

    frame = pd.merge(frame, subset[['sku_id', 'is_product']], how='left')
    frame.fillna(-1, downcast='infer', inplace=True)
    frame = frame[frame.is_product == 1]
    frame_all = frame.copy()
    frame1 = frame.groupby(['user_id'], as_index=False)['sku_id'].agg(__f)
    frame = pd.merge(frame1, frame[['sku_id', 'user_id', 'y', 'pred']], how='left')
    buy_users = frame_all[frame_all.y == 1][['user_id']].drop_duplicates()
    buy_users['y1'] = 1
    frame = pd.merge(frame, buy_users, how='left')
    frame.sort_values(by='pred', inplace=True)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800, 900, 1000, 1100, 1200]:
        try:
            now_frame = frame[0:use_candidate]
            pred_user = len(now_frame)
            correct_frame = now_frame[now_frame.y1 == 1]
            correct_user = len(correct_frame)
            print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
            precision = correct_user / pred_user
            recall = correct_user / buy_user
            F1_1 = 6 * precision * recall / (5 * recall + precision)
            print("F1_1 = %f, " % (F1_1))

            buy_user_sku = buy_user
            pred_user_sku = len(now_frame)
            correct_frame = now_frame[(now_frame.y == 1)]
            correct_user_sku = len(correct_frame)
            print("correct_user_sku = %d, pred_user_sku = %d, buy_user_sku = %d" % (
                correct_user_sku, pred_user_sku, buy_user_sku))
            precision = correct_user_sku / pred_user_sku
            recall = correct_user_sku / buy_user_sku
            F1_2 = 5 * precision * recall / (2 * recall + 3 * precision)
            print("F1_2 = %f, " % (F1_2))
            print("[candidate: %d] Score = %f" % (use_candidate, F1_1 * 0.4 + F1_2 * 0.6))
            print("--------------------------------------------------------------------------------------------------")
        except ZeroDivisionError as e:
            print('Error! No prediction match!')
            pass
        # if use_candidate == 1200:
        #    now_frame[['user_id','sku_id']].to_csv('submit_04_24_2.csv', index=False)

        pass


def run():
    feature_files = ['v1', 'v2', 'y']
    train_frame, train_buy_user = create_dataset('test_set1', feature_files)
    test_frame, test_frame_buy_user = create_dataset('validation_set1', feature_files)

    train_frame.fillna(-1, downcast='infer', inplace=True)
    test_frame.fillna(-1, downcast='infer', inplace=True)
    features = [x for x in filter(lambda x: True if str(x).startswith('_')else False, train_frame.columns)]

    model = XGBClassifier(n_estimators=500, learning_rate=0.05, objective='binary:logistic')
    model.fit(train_frame[features].as_matrix(), train_frame['y'].as_matrix(),
              sample_weight=[15 if x == 1 else 1 for x in train_frame['y'].as_matrix()])
    test_frame['pred'] = [x[0] for x in model.predict_proba(test_frame[features].as_matrix())]
    calculate(test_frame, test_frame_buy_user)
    pass


run()
