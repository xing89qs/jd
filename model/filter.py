#!/usr/bin/env python
# -- coding:utf-8 --

import pandas as pd
import numpy as np

HOUR_WINDOWS = (
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] + list(
        24 * np.array([1, 2, 3, 4, 5, 6, 7, 10, 14, 21, 30, 60])))
DAY_WINDOWS = [1, 2, 3, 7, 10, 14, 21, 30, 60]


def fillna(frame):
    # featureV1,V2,V3
    try:
        for type in range(1, 7):
            frame['_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
            frame['_user_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
            frame['_cate_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
        frame['_all_action_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
    except Exception as e:
        pass

    try:
        for type in range(1, 7):
            frame['_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
            frame['_user_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
            frame['_cate_action_' + str(type) + '_max'].fillna(24 * 60 * 60 * 100, downcast='infer', inplace=True)
    except Exception as e:
        pass

    frame.fillna(-1, downcast='infer', inplace=True)

    pass


def filter(frame):
    fillna(frame)
    pass
