#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from util import *
from util import npz_reader as npz
import numpy as np
import pandas as pd
import xgboost as xgb
from xgboost import XGBClassifier, XGBRegressor
import model.filter as filter
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression


def dfs(l, r, folder, files):
    if l == r:
        # temp_frame = pd.read_csv('../feature_pool/' + folder + '/' + files[l] + '.csv')
        temp_frame = npz.load_npz('../feature_pool/' + folder + '/' + files[l] + '.npz')
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, folder, files)
    r_frame = dfs(mid + 1, r, folder, files)
    ret = pd.merge(l_frame, r_frame, how='left')
    del l_frame
    del r_frame
    return ret


def create_dataset(folder, files, features=None):
    with open('../feature_pool/' + folder + '/buy_cate.txt') as f:
        l = [int(str(x).strip()) for x in f][0]
    data_set = dfs(0, len(files) - 1, folder, files)
    if features is not None:
        data_set1 = data_set[features + ['user_id', 'cate', 'y']]
        del data_set
        return data_set1, l
    else:
        return data_set, l


def __f(x):
    return x.tolist()[0]


def evaluate(now_frame, buy_user, folder=None):
    if folder is not None:
        f = open(folder + '/eval.txt', 'a+')
    else:
        f = None
    try:
        pred_user = len(now_frame)
        correct_frame = now_frame[now_frame.y == 1]
        correct_user = len(correct_frame)
        print("correct_user = %d, pred_user = %d, buy_user = %d" % (correct_user, pred_user, buy_user))
        if f is not None:
            f.write("correct_user = %d, pred_user = %d, buy_user = %d\n" % (correct_user, pred_user, buy_user))
        precision = correct_user / pred_user
        recall = correct_user / buy_user
        F1_1 = 6 * precision * recall / (5 * recall + precision)
        print("F1_1 = %f, " % (F1_1))
        if f is not None:
            f.write("F1_1 = %f, \n" % (F1_1))
        print(
            "--------------------------------------------------------------------------------------------------")
        if f is not None:
            f.write(
                "--------------------------------------------------------------------------------------------------\n")
    except ZeroDivisionError as e:
        print('Error! No prediction match!')
        if f is not None:
            f.write('Error! No prediction match!\n')
    if folder is not None:
        f.close()
    pass


def calculate(frame, buy_user, folder):
    frame = frame[frame.cate == 8]
    frame.sort_values(by='pred', inplace=True)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800]:
        now_frame = frame[0:use_candidate]
        evaluate(now_frame, buy_user, folder)

    frame[['user_id', 'cate', 'pred', 'y']].to_csv(
        folder + '/result' + str(buy_user) + '.csv',
        index=False)

    pass


def weight(x, is_sample=False, sample_weight=50, sample_weight_8=5):
    arg1 = sample_weight_8 if x[0] == 8 else 1
    if is_sample:
        arg2 = 1
    else:
        arg2 = sample_weight if x[1] == 1 else 1
    return arg1 * arg2


def run_model(train_frame, test_frames, folder, set_name, features, is_sample=False, sample_weight=50,
              sample_weight_8=5, **kwargs):
    params = {
        'n_estimators': 500,
        'max_depth': 3,
        'learning_rate': 0.05,
        'objective': 'binary:logistic',
        'subsample': 0.8,
        'colsample_bytree': 0.8,
    }
    for key, value in kwargs.items():
        params[key] = value
    model = XGBClassifier(**params)
    model.fit(train_frame[features].as_matrix(), train_frame['y'].as_matrix(),
              sample_weight=[weight(x, is_sample, sample_weight, sample_weight_8) for x in
                             train_frame[['cate', 'y']].as_matrix()])

    mapFeat = dict(zip(["f" + str(i) for i in range(len(features))], features))
    ts = pd.Series(model.booster().get_fscore())
    ts.index = ts.reset_index()['index'].map(mapFeat)
    with open(folder + '/best_features' + set_name + '.feature', 'w') as file:
        for f in ts.order().index[-300:]:
            file.write(str(f) + ',' + str(ts[f]) + '\n')

    for test_frame, test_frame_buy_user in test_frames:
        test_frame['pred'] = [x[0] for x in model.predict_proba(test_frame[features].as_matrix())]
        print('Results for ' + set_name)
        calculate(test_frame, test_frame_buy_user, folder)
        print('End Results for ' + set_name)


def start_all(train_frame, test_frames, folder, set_name, features):
    run_model(train_frame, test_frames, folder, set_name, features)


def start(train_frame, test_frames, folder, features):
    if not os.path.exists(folder):
        os.mkdir(folder)

    # run_model(train_frame, test_frames, folder, 'model_all_baseline', features)

    _model_cnt = 0
    for depth, sample_weight in [(4, 10), (4, 20), (3, 50)]:
        for seed in [0, 10086]:
            for objective in ['binary:logistic', 'rank:pairwise']:
                for n_estimator, learning_rate in [(500, 0.05), (1000, 0.02), (700, 0.05)]:
                    params = {
                        'n_estimators': n_estimator,
                        'max_depth': depth,
                        'learning_rate': learning_rate,
                        'objective': objective,
                        'subsample': 0.8,
                        'colsample_bytree': 0.8,
                        'seed': seed,
                    }
                    subfolder = folder + '/model_' + str(_model_cnt)
                    if not os.path.exists(subfolder):
                        os.mkdir(subfolder)
                    params_ouput = params.copy()
                    params_ouput['sample_weight'] = sample_weight
                    with open(subfolder + '/params.txt', 'w') as f:
                        f.write(str(params_ouput) + '\n')
                    run_model(train_frame, test_frames, subfolder,
                              'model_' + str(_model_cnt), features, is_sample=False, sample_weight=sample_weight,
                              **params)
                    _model_cnt += 1


def run(train_set, test_set, folder, feature_file='feature1.txt'):
    feature_files = ['data1', 'y']
    features = read_feature(feature_file)
    train_frame = pd.concat(
        [create_dataset(data_set, feature_files, features)[0] for data_set in str(train_set).split(',')])
    test_frames = [create_dataset(data_set, feature_files, features) for data_set in str(test_set).split(',')]
    print(len(train_frame), len(test_frames[0][0]))

    train_frame.fillna(-1, downcast='infer', inplace=True)
    for test_frame, test_frame_buy_user in test_frames:
        test_frame.fillna(-1, downcast='infer', inplace=True)

    start(train_frame, test_frames, folder, features)
    pass


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
