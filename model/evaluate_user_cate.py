#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')

from model.run_user_cate_sample_merge import *
from util import npz_reader as npz

if __name__ == '__main__':

    folders = sys.argv[1]
    folder = folders.split(',')[0]
    buy_user = int(sys.argv[2])

    voting = {}
    _model_cnt = 0
    for depth, sample_weight in [(4, 10), (4, 20), (3, 50)]:
        for seed in [0, 10086]:
            for objective in ['binary:logistic', 'rank:pairwise']:
                for n_estimator, learning_rate in [(500, 0.05), (1000, 0.02), (700, 0.05)]:
                    for folder in folders.split(','):
                        ret_frame = pd.read_csv(
                            folder + '/model_' + str(_model_cnt) + '/result' + str(buy_user) + '.csv')
                        calculate(ret_frame, buy_user, folder + '/model_' + str(_model_cnt))
                        w = 600
                        if depth != 3:
                            w -= 100
                        if objective == "rank:pairwise":
                            w -= 300
                        if n_estimator != 500:
                            w -= 100
                        if seed != 0:
                            w -= 100
                        w = max(w, 100)
                        for user_id in ret_frame['user_id'][0:w].as_matrix():
                            if user_id in voting:
                                voting[user_id] += 1
                            else:
                                voting[user_id] = 1
                    _model_cnt += 1

    y_frame = pd.read_csv(folder + '/model_0' + '/result' + str(buy_user) + '.csv')[['user_id', 'y']]
    final_frame = pd.DataFrame({'user_id': list(voting.keys()), 'count': list(voting.values())})
    final_frame = pd.merge(final_frame, y_frame, how='left')
    final_frame.sort_values(by='count', ascending=False)

    for use_candidate in [100, 200, 300, 400, 500, 600, 650, 700, 750, 800]:
        now_frame = final_frame[0:use_candidate]
        evaluate(now_frame, buy_user, None)
