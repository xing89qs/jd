#!/usr/bin/env python
# -- coding:utf-8 --

import pandas as pd
import os


def process(file_name):
    frame = pd.read_csv('../data/' + file_name)
    frame['date'] = frame['time'].apply(lambda x: str(x)[0:10])
    for date in frame['date'].drop_duplicates().as_matrix():
        if not os.path.exists('../data/action'):
            os.mkdir('../data/action')
        frame[frame.date == date].to_csv('../data/action/' + date + '.csv')


def process_last_day():
    # 02-29 和 03-31
    frame1 = pd.read_csv('../data/' + 'JData_Action_201602.csv')
    frame2 = pd.read_csv('../data/' + 'JData_Action_201603.csv')
    frame1['date'] = frame1['time'].apply(lambda x: str(x)[0:10])
    frame1_0229 = frame1[frame1.date == '2016-02-29']
    del frame1
    frame2['date'] = frame2['time'].apply(lambda x: str(x)[0:10])
    frame2_0229 = frame2[frame2.date == '2016-02-29']
    frame2_0331 = frame2[frame2.date == '2016-03-31']
    del frame2
    frame3 = pd.read_csv('../data/' + 'JData_Action_201604.csv')
    frame3['date'] = frame3['time'].apply(lambda x: str(x)[0:10])
    frame3_0331 = frame3[frame3.date == '2016-03-31']
    del frame3
    frame = pd.concat([frame1_0229, frame2_0229])
    frame.to_csv('../data/action/2016-02-29.csv')
    frame = pd.concat([frame2_0331, frame3_0331])
    frame.to_csv('../data/action/2016-03-31.csv')


if __name__ == "__main__":
    # process('JData_Action_201602.csv')
    # process('JData_Action_201603.csv')
    # process('JData_Action_201604.csv')
    process_last_day()
