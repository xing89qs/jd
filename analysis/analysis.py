#!/usr/bin/env python
# -- coding:utf-8 --

from sklearn.datasets import dump_svmlight_file
import numpy as np
import pandas as pd

frame_04_06 = pd.read_csv('2016-04-06/selected_feature.csv')
frame_04_06.fillna(-1, 0, downcast='infer', inplace=True)
dump_svmlight_file(frame_04_06[[c for c in frame_04_06.columns if str(c).startswith('_')]].as_matrix(),
                   np.array(frame_04_06['y'].as_matrix()), '2016-04-06.svm')

frame_04_10 = pd.read_csv('2016-04-10/selected_feature.csv')
frame_04_10.fillna(-1, 0, downcast='infer', inplace=True)
dump_svmlight_file(frame_04_10[[c for c in frame_04_10.columns if str(c).startswith('_')]].as_matrix(),
                   np.array(frame_04_10['y'].as_matrix()), '2016-04-10.svm')

frame_04_16 = pd.read_csv('2016-04-16/selected_feature.csv')
frame_04_16.fillna(-1, 0, downcast='infer', inplace=True)
dump_svmlight_file(frame_04_16[[c for c in frame_04_16.columns if str(c).startswith('_')]].as_matrix(),
                   np.array(frame_04_16['y'].as_matrix()), '2016-04-16.svm')
