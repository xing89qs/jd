#!/usr/bin/env python
# -- coding:utf-8 --

import itertools


def read_feature(file_path):
    with open(file_path) as f:
        features = []
        cur_feature = ""
        try:
            for line in f:
                line = line.strip()
                if line == "" or str(line).startswith("#"):
                    continue
                if line.startswith('$'):
                    if cur_feature == "":
                        raise Exception('特征不能为空!')
                    key = line[1:].split('=')[0]
                    values = line[1:].split('=')[1].split(',')
                    features[-1][1][key] = values
                elif line.startswith('_'):
                    cur_feature = line
                    features.append((cur_feature, {}))
                else:
                    raise Exception('未知格式 %s' % line)
            feature_set = []
            for f, key_values in features:
                keys = list(key_values.keys())
                for x in itertools.product(*key_values.values()):
                    feature_name = f
                    for i in range(len(x)):
                        feature_name = str(feature_name).replace('${' + keys[i] + '}', x[i])
                    feature_set.append(feature_name)
            return feature_set
        except Exception as e:
            print(e.args)
    return []


read_feature('../model/feature1.txt')
