#!/usr/bin/env python
# -- coding:utf-8 --

import numpy as np
import pandas as pd


def save_npz(dataframe, filename):
    np.savez(filename, columns=dataframe.columns, data=dataframe.as_matrix())
    pass


def load_npz(filename):
    data = np.load(filename)
    ret_frame = pd.DataFrame(data=data['data'], columns=data['columns'])
    del data

    return ret_frame
    pass
