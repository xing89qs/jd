#!/usr/bin/env python
# -- coding:utf-8 --

from scipy.sparse import csr_matrix
import sys
import os

sys.path.append('..')
from util import npz_reader as npz
import time
import pandas as pd
import numpy as np
from scipy import sparse
from sklearn.preprocessing import OneHotEncoder, MultiLabelBinarizer
from sklearn.feature_extraction import DictVectorizer
import util


def load_smat(ft_fp):
    '''
    加载特征文件，特征文件格式如下：
    row_num col_num
    f1_index:f1_value f2_index:f2_value ...
    '''
    data = []
    indice = []
    indptr = [0]
    f = open(ft_fp)
    [row_num, col_num] = [int(num) for num in f.readline().strip().split()]
    for line in f:
        line = line.strip()
        subs = line.split()
        for sub in subs:
            [f_index, f_value] = sub.split(":")
            f_index = int(f_index)
            f_value = float(f_value)
            data.append(f_value)
            indice.append(f_index)
        indptr.append(len(data))
    f.close()
    features = csr_matrix((data, indice, indptr), shape=(row_num, col_num), dtype=float)
    return features


def save_smat(features, ft_pt):
    '''
    存储特征文件
    '''
    (row_num, col_num) = features.shape
    data = features.data
    indice = features.indices
    indptr = features.indptr
    f = open(ft_pt, 'w')
    f.write("%d %d\n" % (row_num, col_num))
    ind_indptr = 1
    begin_line = True
    for ind_data in range(len(data)):
        while ind_data == indptr[ind_indptr]:
            f.write('\n')
            begin_line = True
            ind_indptr += 1
        if (data[ind_data] < 1e-12) and (data[ind_data] > -1e-12):
            continue
        if (not begin_line) and (ind_data != indptr[ind_indptr - 1]):
            f.write(' ')
        f.write("%d:%.4f" % (indice[ind_data], data[ind_data]))
        begin_line = False
    while ind_indptr < len(indptr):
        f.write("\n")
        ind_indptr += 1
    f.close()


def sparse_BlockIndex_df_to_csr(df):
    columns = df.columns
    data, rows = map(list, zip(
        *[(df[col].sp_values - df[col].fill_value, df[col].sp_index.to_int_index().indices) for col in columns]))
    cols = [np.ones_like(a) * i for (i, a) in enumerate(data)]
    data_f = np.concatenate(data)
    rows_f = np.concatenate(rows)
    cols_f = np.concatenate(cols)
    arr = sparse.coo_matrix((data_f, (rows_f, cols_f)), df.shape, dtype=np.float64)
    return arr.tocsr()


def dfs(l, r, dates, columns=None):
    if l == r:
        temp_frame = pd.read_csv('../data/action/' + dates[l] + '.csv')
        temp_frame['user_id'] = temp_frame['user_id'].apply(int)
        if columns is not None:
            temp_frame = temp_frame[columns]
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, dates, columns)
    r_frame = dfs(mid + 1, r, dates, columns)
    return pd.concat([l_frame, r_frame])


def read_action_by_date(dates, columns=None):
    dates = np.array(dates)
    dates = dates[dates >= '2016-02-01']
    print(dates)
    return dfs(0, len(dates) - 1, dates, columns)


def extract_feature(train_date, test_date, func, folder):
    if not os.path.exists('../feature_pool/' + folder):
        os.mkdir('../feature_pool/' + folder)

    # user_id one-hot
    def feature1():
        onehotVec = OneHotEncoder(sparse=True)
        if __DEBUG:
            y_train = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
        else:
            y_train = npz.load_npz('../feature_pool/' + train_date + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + test_date + '/y.npz')
        onehotVec.fit(y_train[['user_id']].as_matrix())
        save_smat(onehotVec.transform(y_train[['user_id']].as_matrix()),
                  '../feature_pool/' + folder + '/' + train_date + '_v1.smat')
        save_smat(onehotVec.transform(y_test[['user_id']].as_matrix()),
                  '../feature_pool/' + folder + '/' + test_date + '_v1.smat')

    # cate one-hot
    def feature2():
        onehotVec = OneHotEncoder(sparse=True)
        if __DEBUG:
            y_train = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
        else:
            y_train = npz.load_npz('../feature_pool/' + train_date + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + test_date + '/y.npz')
        onehotVec.fit(y_train[['cate']].as_matrix())
        save_smat(onehotVec.transform(y_train[['cate']].as_matrix()),
                  '../feature_pool/' + folder + '/' + train_date + '_v2.smat')
        save_smat(onehotVec.transform(y_test[['cate']].as_matrix()),
                  '../feature_pool/' + folder + '/' + test_date + '_v2.smat')

    # user的age
    def feature3():
        onehotVec = MultiLabelBinarizer(sparse_output=True)
        if __DEBUG:
            y_train = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
        else:
            y_train = npz.load_npz('../feature_pool/' + train_date + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + test_date + '/y.npz')

        user_frame = pd.read_csv('../data/JData_User.csv', encoding='gbk')
        y_train = pd.merge(y_train, user_frame, how='left')
        y_test = pd.merge(y_test, user_frame, how='left')
        y_train.fillna(-1, downcast='infer', inplace=True)
        y_test.fillna(-1, downcast='infer', inplace=True)

        onehotVec.fit(y_train[['age']].as_matrix())
        save_smat(onehotVec.transform(y_train[['age']].as_matrix()),
                  '../feature_pool/' + folder + '/' + train_date + '_v3.smat')
        save_smat(onehotVec.transform(y_test[['age']].as_matrix()),
                  '../feature_pool/' + folder + '/' + test_date + '_v3.smat')

    # user 7天内看过的item
    def feature4():
        multiVec = MultiLabelBinarizer(sparse_output=True)
        if __DEBUG:
            y_train = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + 'validation_set2' + '/y.npz')
        else:
            y_train = npz.load_npz('../feature_pool/' + train_date + '/y.npz')
            y_test = npz.load_npz('../feature_pool/' + test_date + '/y.npz')

        if __DEBUG:
            action_dates = util.date_range(util.move_day(train_date, -1), util.move_day(test_date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(train_date, -7), util.move_day(test_date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])

        action_frame_train = action_frame[action_frame.time < train_date]
        user_items_train = \
            action_frame_train[['user_id', 'sku_id']].drop_duplicates().groupby(['user_id'], as_index=False)[
                'sku_id'].agg({'items': lambda x: ",".join(map(str, x.as_matrix()))})

        action_frame_test = action_frame[
            (action_frame.time < test_date) & (action_frame.time >= util.move_day(test_date, -7))]
        user_items_test = \
            action_frame_test[['user_id', 'sku_id']].drop_duplicates().groupby(['user_id'], as_index=False)[
                'sku_id'].agg({'items': lambda x: ",".join(map(str, x.as_matrix()))})

        y_train = pd.merge(y_train, user_items_train, how='left')
        multiVec.fit(action_frame_train[['sku_id']].drop_duplicates().as_matrix())
        train_ret = multiVec.transform(
            list(map(lambda x: [int(y) for y in str(x).split(',')], y_train['items'].as_matrix())))

        y_test = pd.merge(y_test, user_items_test, how='left')
        test_ret = multiVec.transform(
            list(map(lambda x: [int(y) for y in str(x).split(',')], y_test['items'].as_matrix())))
        save_smat(train_ret,
                  '../feature_pool/' + folder + '/' + train_date + '_v4.smat')
        save_smat(test_ret,
                  '../feature_pool/' + folder + '/' + test_date + '_v4.smat')

    eval('feature' + str(func))()


if __name__ == "__main__":
    train_date = sys.argv[1]
    test_date = sys.argv[2]

    __DEBUG = True if int(sys.argv[3]) == 1 else False
    extract_feature(train_date, test_date, sys.argv[4], sys.argv[5])
