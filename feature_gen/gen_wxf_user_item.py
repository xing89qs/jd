#!/usr/bin/env python
# -- coding:utf-8 --


import sys

sys.path.append('../')
import os
import pandas as pd
import util
from util import npz_reader as npz
import logging
import time
import numpy as np

__AUTHOR = 'wxf'  # 作者
__LOG_FILE = 'gen_wxf_user_item.log'


def init():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S',
                        filename=__LOG_FILE,
                        filemode='w')

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # 设置日志打印格式
    formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    # 将定义好的console日志handler添加到root logger
    logging.getLogger('').addHandler(console)

    if not os.path.exists('../feature_pool/' + __DATASET):
        os.mkdir('../feature_pool/' + __DATASET)


def dfs(l, r, dates, columns=None):
    if l == r:
        temp_frame = pd.read_csv('../data/action/' + dates[l] + '.csv')
        temp_frame['user_id'] = temp_frame['user_id'].apply(int)
        if columns is not None:
            temp_frame = temp_frame[columns]
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, dates, columns)
    r_frame = dfs(mid + 1, r, dates, columns)
    return pd.concat([l_frame, r_frame])


def read_action_by_date(dates, columns=None):
    dates = np.array(dates)
    dates = dates[dates >= '2016-02-01']
    print(dates)
    return dfs(0, len(dates) - 1, dates, columns)


TIME2STAMP_MAP = {}


def __time2stamp(x, reg='%Y-%m-%d %H:%M:%S'):
    if x in TIME2STAMP_MAP:
        return TIME2STAMP_MAP[x]
    else:
        try:
            ret = int(time.mktime(time.strptime(x, reg)))
            TIME2STAMP_MAP[x] = ret
        except Exception as e:
            return -1
        return ret


def __div(a, b):
    if b == 0 or np.isnan(b) or b is None:
        return -1
    try:
        return a / float(b)
    except Exception as e:
        return -1


def __mean(*args, **kwargs):
    try:
        return np.mean(*args, **kwargs)
    except Exception as e:
        return -1


def __hour_segment(x):
    hour = str(x)[-8:-6]
    if '00' <= hour <= '08':
        return 0
    elif hour <= '18':
        return 1
    return 2


def featureY(date, offset=7):
    """
    :param date:
    :param offset:
        表示选取[date-offset,date-1]作为候选集合, [date,date+4]来求label
    :return:
    """
    logger.info('提取label...')
    logger.info('offset = %d' % (offset))
    logger.info('候选日期 [ %s ~ %s ]' % (util.move_day(date, -offset), util.move_day(date, -1)))
    logger.info('读取候选日期的action文件')
    if __DEBUG:
        candidates_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)
    else:
        candidates_dates = util.date_range(util.move_day(date, -offset), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)

    logger.info('下单日期 [ %s ~ %s ]' % (util.move_day(date, 0), util.move_day(date, 4)))
    logger.info('读取下单日期的action文件')
    if date != '2016-04-16':
        pay_dates = util.date_range(date, util.move_day(date, 4))
        pay_frame = read_action_by_date(pay_dates)
        pay_frame = pay_frame[pay_frame.type == 4]

        candidates_frame['flag'] = 1

        new_frame = pd.merge(pay_frame[['user_id', 'sku_id', 'cate']].drop_duplicates(),
                             candidates_frame[['user_id', 'sku_id', 'cate', 'flag']].drop_duplicates(), how='left')
        new_frame.fillna(-1, downcast='infer', inplace=True)
        logger.info("下单的不同user&item对共 %d 对, 召回了 %d 对, 召回率 %f ." % (
            len(new_frame), len(new_frame[new_frame.flag == 1]), len(new_frame[new_frame.flag == 1]) / len(new_frame)))
        with open('../feature_pool/' + __DATASET + '/buy_item.txt', 'w') as f:
            frame_pay_cate_8 = pay_frame[pay_frame.cate == 8]['user_id'].drop_duplicates()
            l = len(frame_pay_cate_8)
            logger.info("在cate = 8类别下下单的不同user共 %d " % (l))
            f.write(str(l))

        # 构造样本
        sample_frame = candidates_frame[['sku_id', 'user_id', 'cate']].drop_duplicates()
        sample_frame = sample_frame[sample_frame.cate == 8]
        pay_frame['y'] = 1
        pay_frame = pay_frame[['sku_id', 'user_id', 'cate', 'y']].drop_duplicates()
        y_frame = pd.merge(sample_frame, pay_frame[['cate', 'sku_id', 'user_id', 'y']], how='left')
        y_frame.fillna(0, downcast='infer', inplace=True)

        # 抽样一下
        # y_frame = pd.concat([y_frame[y_frame.y == 1], y_frame[y_frame.y == 0].sample(frac=0.2, random_state=0)])
        # assert (len(sample_frame) == len(y_frame))
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('正例子 %d条, 占%f ' % (len(y_frame[y_frame.y == 1]), len(y_frame[y_frame.y == 1]) / len(y_frame)))
    else:
        sample_frame = candidates_frame[['sku_id', 'cate', 'user_id']].drop_duplicates()
        sample_frame = sample_frame[sample_frame.cate == 8]
        y_frame = sample_frame
        y_frame['y'] = 0
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('线上测试集Y_Frame生成完成')
    # y_frame.to_csv('../feature_pool/' + __DATASET + '/y.csv', index=False)
    npz.save_npz(y_frame, '../feature_pool/' + __DATASET + '/y.npz')
    pass


def split_feature(frame, feature_name, feature_list):
    assert (len(frame[feature_name].as_matrix()[0].split(';')) == len(feature_list))
    for i in range(len(feature_list)):
        name = feature_list[i]
        frame[name] = frame[feature_name].apply(lambda x: float(x.split(';')[i]))


def extract_featrure(date, f=None):
    # y_frame = pd.read_csv('../feature_pool/' + __DATASET + '/y.csv')
    y_frame = npz.load_npz('../feature_pool/' + __DATASET + '/y.npz')
    DAY_WINDOWS = [1, 2, 3, 7, 10, 14, 21, 30, 60]
    HOUR_WINDOWS = [1, 2, 3, 4, 6, 12] + list(24 * np.array(DAY_WINDOWS))
    target_stamp = time.mktime(time.strptime(date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))

    def __feature1(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV1():
        __FEATURE_FILE_NAME = 'v1.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])

        v1_frame = y_frame[['cate', 'sku_id', 'user_id']]
        action_frame = pd.merge(v1_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['sku_id', 'cate', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v1': __feature1})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v1', feature_list)
            v1_frame = pd.merge(v1_frame, ret_frame[['cate', 'sku_id', 'user_id'] + feature_list], how='left')
            del action_type_frame
            del user_sku_group
            del ret_frame
        assert (len(v1_frame) == len(y_frame))
        # v1_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v1_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v1_frame
        pass

    def __feature2(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV2():
        __FEATURE_FILE_NAME = 'v2.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])

        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))
        v2_frame = y_frame[['cate', 'sku_id', 'user_id']]

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            sku_group = action_type_frame.groupby(['sku_id'], as_index=False)
            ret_frame = sku_group['time'].agg({'_v2': __feature2})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_item_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v2', feature_list)
            v2_frame = pd.merge(v2_frame, ret_frame[['sku_id'] + feature_list], how='left')
            del ret_frame
            del action_type_frame
            del sku_group
        # v2_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v2_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)

        assert (len(v2_frame) == len(y_frame))
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v2_frame
        pass

    def featureV3():
        __FEATURE_FILE_NAME = 'v3.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v2.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v2.npz')

        v3_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v3_frame = pd.merge(v3_frame, base_frame, how='left')

        assert (len(y_frame) == len(v3_frame))
        # v3_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v3_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v3_frame
        pass

    def featureV4():
        __FEATURE_FILE_NAME = 'v4.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v3.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v3.npz')

        v4_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v4_frame = pd.merge(v4_frame, base_frame, how='left')

        assert (len(y_frame) == len(v4_frame))
        # v4_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v4_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v4_frame
        pass

    rank_map = {}

    def __feature5(x):
        return None

    def featureV5():
        __FEATURE_FILE_NAME = 'v5.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-特征行为的统计特征')

        v5_frame = y_frame[['user_id', 'sku_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')

        base_frame.fillna(0, downcast='infer', inplace=True)
        base_frame = pd.merge(base_frame[['user_id', 'sku_id']], base_frame.rename(columns={'sku_id': 'sku_id1'}),
                              how='left', on=['user_id'])
        columns = [f for f in base_frame.columns if str(f).startswith('_')]

        for f in columns:
            rank_map = {}
            base_frame.groupby(['user_id', 'cate'])
            base_frame[f + '_rank_in_cate'] = base_frame[f].apply(
                lambda x: rank_map[str(x['user_id']) + ':' + str(x['sku_id'])])
        base_frame = base_frame[['user_id', 'sku_id', 'cate'] + ['_other_cate_max' + f for f in columns]]

        v5_frame = pd.merge(v5_frame, base_frame, how='left')
        v5_frame.fillna(0, downcast='infer', inplace=True)
        assert (len(y_frame) == len(v5_frame))
        # v5_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v5_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def featureV6():
        __FEATURE_FILE_NAME = 'v6.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v5.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v5.npz')

        v6_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v6_frame = pd.merge(v6_frame, base_frame, how='left')

        assert (len(y_frame) == len(v6_frame))
        # v4_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v6_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v6_frame
        pass

    def featureV7():
        __FEATURE_FILE_NAME = 'v7.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v8.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v8.npz')

        v7_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v7_frame = pd.merge(v7_frame, base_frame, how='left')

        assert (len(y_frame) == len(v7_frame))
        # v7_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v7_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v7_frame
        pass

    def featureV8():
        __FEATURE_FILE_NAME = 'v8.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        v8_frame = y_frame[['cate', 'sku_id', 'user_id']]
        comment_frame = pd.read_csv('../data/JData_Comment.csv')
        comment_frame = comment_frame[comment_frame.dt < date]
        comment_frame.sort_values(by='dt', inplace=True)
        comment_frame = comment_frame.groupby(['sku_id'], as_index=False)[
            'comment_num', 'has_bad_comment', 'bad_comment_rate'].agg(lambda x: x.as_matrix()[-1])
        comment_frame.rename(columns={'comment_num': '_comment_num', 'has_bad_comment': '_has_bad_comment',
                                      'bad_comment_rate': '_bad_comment_rate'}, inplace=True)
        v8_frame = pd.merge(v8_frame, comment_frame, how='left')
        # print(v8_frame)
        assert (len(y_frame) == len(v8_frame))
        # v8_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v8_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v8_frame
        pass

    def __feature9(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV9():
        __FEATURE_FILE_NAME = 'v9.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])

        v9_frame = y_frame[['sku_id', 'cate', 'user_id']]
        action_frame = pd.merge(v9_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取所有操作的统计特征')
        user_sku_group = action_frame.groupby(['cate', 'sku_id', 'user_id'], as_index=False)
        ret_frame = user_sku_group['time'].agg({'_v9': __feature9})
        feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
        feature_list = ['_all_action_' + str(x) for x in feature_list]
        split_feature(ret_frame, '_v9', feature_list)
        v9_frame = pd.merge(v9_frame, ret_frame[['cate', 'sku_id', 'user_id'] + feature_list], how='left')
        del action_frame
        del user_sku_group
        del ret_frame
        assert (len(v9_frame) == len(y_frame))
        # v9_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v9_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v9_frame
        pass

    def featureV10():
        __FEATURE_FILE_NAME = 'v10.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -2), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])

        v10_frame = y_frame[['cate', 'sku_id', 'user_id']]
        action_frame = pd.merge(v10_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))
        action_frame.sort_values(by='time', inplace=True)
        sequences = np.zeros(len(action_frame))
        time_map = {}
        SECONDS_PER_HOUR = 60 * 60
        i = 0
        for row in action_frame.itertuples():
            if row[2] not in time_map:
                seq = 0
                last = -100000000
            else:
                seq, last = time_map[row[2]]
            if row[4] - last >= SECONDS_PER_HOUR:
                seq += 1
            sequences[i] = seq
            last = row[4]
            i += 1
            time_map[row[2]] = (seq, last)
        action_frame['sequence'] = sequences

        v10_frame = y_frame[['user_id', 'sku_id', 'cate']]

        for hour in HOUR_WINDOWS:
            action_hour_frame = action_frame[action_frame.time >= target_stamp - SECONDS_PER_HOUR * hour]
            action_sku_group = action_hour_frame.groupby(['user_id', 'cate', 'sku_id'], as_index=False)
            action_user_cate_frame = action_sku_group['sequence'].agg(
                {'_action_' + str(hour) + 'hour_seq_count': lambda x: len(np.unique(x))})
            action_user_group = action_hour_frame.groupby(['user_id'], as_index=False)
            action_user_frame = action_user_group['sequence'].agg(
                {'_user_action_' + str(hour) + 'hour_seq_count': lambda x: len(np.unique(x))})
            v10_frame = pd.merge(v10_frame, action_user_cate_frame, how='left')
            v10_frame = pd.merge(v10_frame, action_user_frame, how='left')
            v10_frame['_user_action_' + str(hour) + 'hour_visit_sku_rate'] = v10_frame.apply(
                lambda x: None if x['_user_action_' + str(hour) + 'hour_seq_count'] == 0 or
                                  np.isnan(x['_user_action_' + str(hour) + 'hour_seq_count']) \
                    else x['_action_' + str(hour) + 'hour_seq_count'] /
                         x['_user_action_' + str(hour) + 'hour_seq_count']
                , axis=1)

        assert (len(v10_frame) == len(y_frame))
        # v10_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v10_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v10_frame
        pass

    def featureV11():
        __FEATURE_FILE_NAME = 'v11.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v28.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v28.npz')

        v11_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v11_frame = pd.merge(v11_frame, base_frame, how='left')

        assert (len(y_frame) == len(v11_frame))
        # v11_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v11_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v11_frame
        pass

    def featureV12():
        __FEATURE_FILE_NAME = 'v12.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')

        v12_frame = y_frame[['cate', 'sku_id', 'user_id']]
        product_frame = pd.read_csv('../data/JData_Product.csv')
        v12_frame = pd.merge(v12_frame, product_frame, how='left')
        for i in [1, 2, 3]:
            v12_frame['_is_a1_' + str(i)] = v12_frame['a1'].apply(lambda x: 1 if x == i else 0)
        for i in [1, 2]:
            v12_frame['_is_a2_' + str(i)] = v12_frame['a2'].apply(lambda x: 1 if x == i else 0)
            v12_frame['_is_a3_' + str(i)] = v12_frame['a3'].apply(lambda x: 1 if x == i else 0)

        v12_frame = v12_frame[['user_id', 'sku_id', 'cate'] + [f for f in v12_frame.columns if str(f).startswith('_')]]

        assert (len(y_frame) == len(v12_frame))
        # v12_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v12_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v12_frame
        pass

    def featureV13():
        __FEATURE_FILE_NAME = 'v13.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v1.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v1.npz')

        v13_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v13_frame = pd.merge(v13_frame, base_frame, how='left')

        columns_map = dict(
            zip(v13_frame.columns, [str(x).replace('action', 'user_cate_action') for x in v13_frame.columns]))
        v13_frame = v13_frame.rename(columns=columns_map)

        assert (len(y_frame) == len(v13_frame))
        # v13_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v13_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v13_frame
        pass

    def __feature14(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV14():
        __FEATURE_FILE_NAME = 'v14.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'sku_id', 'user_id', 'type', 'time', 'brand'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'sku_id', 'user_id', 'type', 'time', 'brand'])

        v14_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v14_frame = pd.merge(v14_frame, action_frame[['sku_id', 'brand']].drop_duplicates(), how='left')
        assert (len(v14_frame) == len(y_frame))
        action_frame = pd.merge(v14_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['brand', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v14': __feature14})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_brand_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v14', feature_list)
            v14_frame = pd.merge(v14_frame, ret_frame[['brand', 'user_id'] + feature_list], how='left')
            del action_type_frame
            del user_sku_group
            del ret_frame
        assert (len(v14_frame) == len(y_frame))
        # v1_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v14_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v14_frame
        pass

    def featureV15():
        __FEATURE_FILE_NAME = 'v15.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            base_frame = npz.load_npz('../feature_pool/' + 'validation_set2/' + 'v36.npz')
        else:
            base_frame = npz.load_npz('../feature_pool/' + date + '/' + 'v36.npz')

        v15_frame = y_frame[['cate', 'sku_id', 'user_id']]
        v15_frame = pd.merge(v15_frame, base_frame, how='left')

        assert (len(y_frame) == len(v15_frame))
        # v13_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v15_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v15_frame
        pass

    f_map = {
        1: featureV1,
        2: featureV2,
        3: featureV3,
        4: featureV4,
        5: featureV5,
        6: featureV6,
        7: featureV7,
        8: featureV8,
        9: featureV9,
        10: featureV10,
        11: featureV11,
        12: featureV12,
        13: featureV13,
        14: featureV14,
        15: featureV15,
    }

    if f is not None:
        f_map[int(f)]()
    pass


if __name__ == "__main__":
    logger = logging.getLogger('gen_wxf')
    logger.setLevel(logging.DEBUG)

    date = sys.argv[1]
    offset = int(sys.argv[2])
    __DATASET = sys.argv[3]  # 样本集
    __DEBUG = False if len(sys.argv) > 4 and int(sys.argv[4]) == 0 else True
    init()

    pass
    # 初始化日志信息

    # featureY(date, offset)
    extract_featrure(date, sys.argv[5] if len(sys.argv) > 5 else None)
