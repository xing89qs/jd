#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('../')
import os
import pandas as pd
import util
from util import npz_reader as npz
import logging
import time
import numpy as np

__AUTHOR = 'wxf'  # 作者
__LOG_FILE = 'gen_wxf_user_cate.log'


def init():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S',
                        filename=__LOG_FILE,
                        filemode='w')

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # 设置日志打印格式
    formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    # 将定义好的console日志handler添加到root logger
    logging.getLogger('').addHandler(console)

    if not os.path.exists('../feature_pool/' + __DATASET):
        os.mkdir('../feature_pool/' + __DATASET)


def dfs(l, r, dates, columns=None):
    if l == r:
        temp_frame = pd.read_csv('../data/action/' + dates[l] + '.csv')
        temp_frame['user_id'] = temp_frame['user_id'].apply(int)
        if columns is not None:
            temp_frame = temp_frame[columns]
        return temp_frame
    mid = int((l + r) / 2)
    l_frame = dfs(l, mid, dates, columns)
    r_frame = dfs(mid + 1, r, dates, columns)
    return pd.concat([l_frame, r_frame])


def read_action_by_date(dates, columns=None):
    dates = np.array(dates)
    dates = dates[dates >= '2016-02-01']
    print(dates)
    return dfs(0, len(dates) - 1, dates, columns)


TIME2STAMP_MAP = {}


def __time2stamp(x, reg='%Y-%m-%d %H:%M:%S'):
    if x in TIME2STAMP_MAP:
        return TIME2STAMP_MAP[x]
    else:
        try:
            ret = int(time.mktime(time.strptime(x, reg)))
            TIME2STAMP_MAP[x] = ret
        except Exception as e:
            return -1
        return ret


def __div(a, b):
    if b == 0 or np.isnan(b) or b is None:
        return -1
    try:
        return a / float(b)
    except Exception as e:
        return -1


def __mean(*args, **kwargs):
    try:
        return np.mean(*args, **kwargs)
    except Exception as e:
        return -1


def __hour_segment(x):
    hour = str(x)[-8:-6]
    if '00' <= hour <= '08':
        return 0
    elif hour <= '18':
        return 1
    return 2


def featureY(date, offset=7):
    """
    :param date:
    :param offset:
        表示选取[date-offset,date-1]作为候选集合, [date,date+4]来求label
    :return:
    """
    logger.info('提取label...')
    logger.info('offset = %d' % (offset))
    logger.info('候选日期 [ %s ~ %s ]' % (util.move_day(date, -offset), util.move_day(date, -1)))
    logger.info('读取候选日期的action文件')
    if __DEBUG:
        candidates_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)
    else:
        candidates_dates = util.date_range(util.move_day(date, -offset), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)

    logger.info('下单日期 [ %s ~ %s ]' % (util.move_day(date, 0), util.move_day(date, 4)))
    logger.info('读取下单日期的action文件')
    if date != '2016-04-16':
        pay_dates = util.date_range(date, util.move_day(date, 4))
        pay_frame = read_action_by_date(pay_dates)
        pay_frame = pay_frame[pay_frame.type == 4]

        candidates_frame['flag'] = 1

        new_frame = pd.merge(pay_frame[['user_id', 'cate']].drop_duplicates(),
                             candidates_frame[['user_id', 'cate', 'flag']].drop_duplicates(), how='left')
        new_frame.fillna(-1, downcast='infer', inplace=True)
        logger.info("下单的不同user&cate对共 %d 对, 召回了 %d 对, 召回率 %f ." % (
            len(new_frame), len(new_frame[new_frame.flag == 1]), len(new_frame[new_frame.flag == 1]) / len(new_frame)))
        with open('../feature_pool/' + __DATASET + '/buy_cate.txt', 'w') as f:
            frame_pay_cate_8 = pay_frame[pay_frame.cate == 8]['user_id'].drop_duplicates()
            l = len(frame_pay_cate_8)
            logger.info("在cate = 8类别下下单的不同user共 %d " % (l))
            f.write(str(l))

        # 构造样本
        sample_frame = candidates_frame[['cate', 'user_id']].drop_duplicates()
        pay_frame['y'] = 1
        pay_frame = pay_frame[['cate', 'user_id', 'y']].drop_duplicates()
        y_frame = pd.merge(sample_frame, pay_frame[['cate', 'user_id', 'y']], how='left')
        y_frame.fillna(0, downcast='infer', inplace=True)
        assert (len(sample_frame) == len(y_frame))
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('正例子 %d条, 占%f ' % (len(y_frame[y_frame.y == 1]), len(y_frame[y_frame.y == 1]) / len(y_frame)))
    else:
        sample_frame = candidates_frame[['cate', 'user_id']].drop_duplicates()
        y_frame = sample_frame
        y_frame['y'] = 0
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('线上测试集Y_Frame生成完成')
    # y_frame.to_csv('../feature_pool/' + __DATASET + '/y.csv', index=False)
    npz.save_npz(y_frame, '../feature_pool/' + __DATASET + '/y.npz')
    pass


def featureY1(date, offset=7):
    """
    :param date:
    :param offset:
        表示选取[date-offset,date-1]作为候选集合, [date,date]来求label
    :return:
    """
    logger.info('提取label...')
    logger.info('offset = %d' % (offset))
    logger.info('候选日期 [ %s ~ %s ]' % (util.move_day(date, -offset), util.move_day(date, -1)))
    logger.info('读取候选日期的action文件')
    if __DEBUG:
        candidates_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)
    else:
        candidates_dates = util.date_range(util.move_day(date, -offset), util.move_day(date, -1))
        candidates_frame = read_action_by_date(candidates_dates)

    logger.info('下单日期 [ %s ~ %s ]' % (util.move_day(date, 0), util.move_day(date, 0)))
    logger.info('读取下单日期的action文件')
    if date != '2016-04-16':
        pay_dates = util.date_range(date, util.move_day(date, 0))
        pay_frame = read_action_by_date(pay_dates)
        pay_frame = pay_frame[pay_frame.type == 4]

        candidates_frame['flag'] = 1

        new_frame = pd.merge(pay_frame[['user_id', 'cate']].drop_duplicates(),
                             candidates_frame[['user_id', 'cate', 'flag']].drop_duplicates(), how='left')
        new_frame.fillna(-1, downcast='infer', inplace=True)
        logger.info("下单的不同user&cate对共 %d 对, 召回了 %d 对, 召回率 %f ." % (
            len(new_frame), len(new_frame[new_frame.flag == 1]), len(new_frame[new_frame.flag == 1]) / len(new_frame)))
        with open('../feature_pool/' + __DATASET + '/buy_cate.txt', 'w') as f:
            frame_pay_cate_8 = pay_frame[pay_frame.cate == 8]['user_id'].drop_duplicates()
            l = len(frame_pay_cate_8)
            logger.info("在cate = 8类别下下单的不同user共 %d " % (l))
            f.write(str(l))

        # 构造样本
        sample_frame = candidates_frame[['cate', 'user_id']].drop_duplicates()
        pay_frame['y1'] = 1
        pay_frame = pay_frame[['cate', 'user_id', 'y1']].drop_duplicates()
        y_frame = pd.merge(sample_frame, pay_frame[['cate', 'user_id', 'y1']], how='left')
        y_frame.fillna(0, downcast='infer', inplace=True)
        assert (len(sample_frame) == len(y_frame))
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('正例子 %d条, 占%f ' % (len(y_frame[y_frame.y1 == 1]), len(y_frame[y_frame.y1 == 1]) / len(y_frame)))
    else:
        sample_frame = candidates_frame[['cate', 'user_id']].drop_duplicates()
        y_frame = sample_frame
        y_frame['y1'] = 0
        logger.info('样本数 %d ' % (len(y_frame)))
        logger.info('线上测试集Y_Frame生成完成')
    # y_frame.to_csv('../feature_pool/' + __DATASET + '/y1.csv', index=False)
    npz.save_npz(y_frame, '../feature_pool/' + __DATASET + '/y1.npz')
    pass


def split_feature(frame, feature_name, feature_list):
    assert (len(frame) == 0 or len(frame[feature_name].as_matrix()[0].split(';')) == len(feature_list))
    for i in range(len(feature_list)):
        name = feature_list[i]
        frame[name] = frame[feature_name].apply(lambda x: float(x.split(';')[i]))


def extract_featrure(date, f=None):
    # y_frame = pd.read_csv('../feature_pool/' + __DATASET + '/y.csv')
    y_frame = npz.load_npz('../feature_pool/' + __DATASET + '/y.npz')
    DAY_WINDOWS = [1, 2, 3, 7, 10, 14, 21, 30, 60]
    HOUR_WINDOWS = [1, 2, 3, 4, 6, 12] + list(24 * np.array(DAY_WINDOWS))
    target_stamp = time.mktime(time.strptime(date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))

    def __feature1(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV1():
        __FEATURE_FILE_NAME = 'v1.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v1_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v1_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['cate', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v1': __feature1})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v1', feature_list)
            v1_frame = pd.merge(v1_frame, ret_frame[['cate', 'user_id'] + feature_list], how='left')
            del action_type_frame
            del user_sku_group
            del ret_frame
        assert (len(v1_frame) == len(y_frame))
        # v1_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v1_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v1_frame
        pass

    def __feature2(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV2():
        __FEATURE_FILE_NAME = 'v2.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))
        v2_frame = y_frame[['cate', 'user_id']]

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            sku_group = action_type_frame.groupby(['cate'], as_index=False)
            ret_frame = sku_group['time'].agg({'_v2': __feature2})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_cate_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v2', feature_list)
            v2_frame = pd.merge(v2_frame, ret_frame[['cate'] + feature_list], how='left')
            del ret_frame
            del action_type_frame
            del sku_group
        # v2_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v2_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)

        assert (len(v2_frame) == len(y_frame))
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v2_frame
        pass

    def __feature3(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV3():
        __FEATURE_FILE_NAME = 'v3.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])[-10000:]
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v3_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v3_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_group = action_type_frame.groupby(['user_id'], as_index=False)
            ret_frame = user_group['time'].agg({'_v3': __feature3})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_user_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v3', feature_list)
            v3_frame = pd.merge(v3_frame, ret_frame[['user_id'] + feature_list], how='left')
            del action_type_frame
            del user_group
            del ret_frame
        assert (len(y_frame) == len(v3_frame))
        # v3_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v3_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v3_frame
        pass

    def featureV4():
        __FEATURE_FILE_NAME = 'v4.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-特征行为的统计特征')

        v4_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')

        base_frame.fillna(0, downcast='infer', inplace=True)
        base_frame = pd.merge(base_frame[['user_id', 'cate']], base_frame.rename(columns={'cate': 'cate1'}), how='left',
                              on=['user_id'])
        columns = [f for f in base_frame.columns if str(f).startswith('_')]

        same_cate_frame = base_frame[base_frame.cate == base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                  as_index=False).agg(np.max)
        ret_frame = v4_frame
        other_cate_frame = base_frame[base_frame.cate != base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                   as_index=False).agg(np.max)
        other_cate_frame.rename(columns={f: f + '_other' for f in columns}, inplace=True)
        ret_frame = pd.merge(ret_frame, other_cate_frame, how='left', on=['user_id', 'cate'])
        ret_frame.fillna(0, downcast='infer', inplace=True)
        ret_frame = pd.merge(ret_frame, same_cate_frame, how='left', on=['user_id', 'cate'])
        for f in columns:
            ret_frame['_other_cate_max' + f] = ret_frame[f] - ret_frame[f + '_other']
        ret_frame = ret_frame[['user_id', 'cate'] + ['_other_cate_max' + f for f in columns]]

        v4_frame = pd.merge(v4_frame, ret_frame, how='left')
        v4_frame.fillna(0, downcast='infer', inplace=True)
        assert (len(y_frame) == len(v4_frame))
        # v4_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v4_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame
        del ret_frame

        pass

    def featureV5():
        __FEATURE_FILE_NAME = 'v5.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-特征行为的统计特征')

        v5_frame = y_frame[['user_id', 'cate']]
        all_cates = v5_frame['cate'].drop_duplicates().tolist()
        for cate in all_cates:
            v5_frame['_is_cate' + str(cate)] = v5_frame['cate'].apply(lambda x: 1 if x == cate else 0)

        # v5_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v5_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)

        pass

    def featureV6():
        __FEATURE_FILE_NAME = 'v6.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-特征行为的统计特征')

        v6_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        base_frame = pd.merge(base_frame[['user_id', 'cate']], base_frame.rename(columns={'cate': 'cate1'}), how='left',
                              on=['user_id'])
        columns = [f for f in base_frame.columns if str(f).startswith('_')]

        same_cate_frame = base_frame[base_frame.cate == base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                  as_index=False).agg(np.mean)
        ret_frame = v6_frame
        other_cate_frame = base_frame[base_frame.cate != base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                   as_index=False).agg(np.mean)
        other_cate_frame.rename(columns={f: f + '_other' for f in columns}, inplace=True)
        ret_frame = pd.merge(ret_frame, other_cate_frame, how='left', on=['user_id', 'cate'])
        ret_frame.fillna(0, downcast='infer', inplace=True)
        ret_frame = pd.merge(ret_frame, same_cate_frame, how='left', on=['user_id', 'cate'])
        for f in columns:
            ret_frame['_other_cate_avg' + f] = ret_frame[f] - ret_frame[f + '_other']
        ret_frame = ret_frame[['user_id', 'cate'] + ['_other_cate_avg' + f for f in columns]]

        v6_frame = pd.merge(v6_frame, ret_frame, how='left')
        assert (len(y_frame) == len(v6_frame))
        # v6_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v6_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        print(v6_frame)
        del base_frame
        del ret_frame

        pass

    def featureV7():
        __FEATURE_FILE_NAME = 'v7.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -2), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v7_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v7_frame, action_frame, how='left')
        action_frame['date'] = action_frame['time'].apply(lambda x: x[:10])
        action_frame_count = action_frame.groupby(['user_id', 'date', 'type']).apply(np.size).reset_index(name='count')
        del action_frame

        logger.info('提取每种操作的统计特征')
        for day in DAY_WINDOWS:
            day_frame = action_frame_count[action_frame_count.date >= util.move_day(date, -day)]
            for type in range(1, 7):
                day_type_frame = day_frame[day_frame.type == type]
                ret_frame = day_type_frame.groupby(['user_id'], as_index=False)['count'].agg(
                    {'_user_' + str(day) + 'day_action_' + str(type) + '_avg': np.mean,
                     '_user_' + str(day) + 'day_action_' + str(type) + '_std': np.std,
                     '_user_' + str(day) + 'day_action_' + str(type) + '_min': np.min})
                v7_frame = pd.merge(v7_frame, ret_frame, how='left')
                assert (len(v7_frame) == len(y_frame))
                del ret_frame
                del day_type_frame
        # v7_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v7_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v7_frame
        del action_frame_count
        pass

    def __featureV8(x):
        x = str(x)
        if '15' in x:
            return 0
        if '16' in x:
            return 1
        if '26' in x:
            return 2
        if '36' in x:
            return 3
        if '46' in x:
            return 4
        if '56' in x:
            return 5
        return -1

    def featureV8():
        __FEATURE_FILE_NAME = 'v8.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户特征')

        v8_frame = y_frame[['cate', 'user_id']]
        user_frame = pd.read_csv('../data/JData_User.csv', encoding='gbk')
        user_frame['_age'] = user_frame['age'].apply(__featureV8)
        v8_frame = pd.merge(v8_frame, user_frame, how='left')

        v8_frame['_user_lv_cd'] = v8_frame['user_lv_cd']
        v8_frame['_user_reg_dates'] = v8_frame['user_reg_tm'].apply(lambda x: util.time_diff(date, x))
        for sex in [0, 1, 2]:
            v8_frame['_is_sex' + str(sex)] = v8_frame['sex'].apply(lambda x: 1 if x == sex else 0)
        ages_list = user_frame['age'].drop_duplicates().tolist()
        for age in range(len(ages_list)):
            v8_frame['_is_age' + str(age)] = v8_frame['age'].apply(lambda x: 1 if str(x) == ages_list[age]else 0)
        columns = [x for x in v8_frame if str(x).startswith('_')]
        v8_frame = v8_frame[columns + ['user_id', 'cate']]

        # v8_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v8_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)

        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v8_frame

    def featureV9():
        __FEATURE_FILE_NAME = 'v9.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-特征行为的统计特征')

        v9_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')

        base_frame = pd.merge(base_frame[['user_id', 'cate']], base_frame.rename(columns={'cate': 'cate1'}), how='left',
                              on=['user_id'])
        columns = [f for f in base_frame.columns if str(f).startswith('_')]

        same_cate_frame = base_frame[base_frame.cate == base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                  as_index=False).agg(np.min)
        ret_frame = v9_frame
        other_cate_frame = base_frame[base_frame.cate != base_frame.cate1].groupby(['user_id', 'cate'],
                                                                                   as_index=False).agg(np.min)
        other_cate_frame.rename(columns={f: f + '_other' for f in columns}, inplace=True)
        ret_frame = pd.merge(ret_frame, other_cate_frame, how='left', on=['user_id', 'cate'])
        ret_frame.fillna(0, downcast='infer', inplace=True)
        ret_frame = pd.merge(ret_frame, same_cate_frame, how='left', on=['user_id', 'cate'])
        for f in columns:
            ret_frame['_other_cate_min' + f] = ret_frame[f] - ret_frame[f + '_other']
        ret_frame = ret_frame[['user_id', 'cate'] + ['_other_cate_min' + f for f in columns]]

        v9_frame = pd.merge(v9_frame, ret_frame, how='left')
        assert (len(y_frame) == len(v9_frame))
        # v9_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v9_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame
        del ret_frame
        pass

    def __feature10(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV10():
        __FEATURE_FILE_NAME = 'v10.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v10_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v10_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取所有操作的统计特征')
        user_sku_group = action_frame.groupby(['cate', 'user_id'], as_index=False)
        ret_frame = user_sku_group['time'].agg({'_v10': __feature10})
        feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
        feature_list = ['_all_action_' + str(x) for x in feature_list]
        split_feature(ret_frame, '_v10', feature_list)
        v10_frame = pd.merge(v10_frame, ret_frame[['cate', 'user_id'] + feature_list], how='left')
        del action_frame
        del user_sku_group
        del ret_frame
        assert (len(v10_frame) == len(y_frame))
        # v10_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v10_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v10_frame
        pass

    def __feature11(x):
        features = []

        features.append(int(target_stamp - np.min(x)))
        features.append(int(target_stamp - np.max(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV11():
        __FEATURE_FILE_NAME = 'v11.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        action_frame = action_frame.drop_duplicates()
        v11_frame = y_frame[['cate', 'user_id']]
        action_frame = action_frame.drop_duplicates()
        action_frame = pd.merge(v11_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['cate', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v11': __feature11})
            feature_list = ['min', 'max', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_action_dropdup_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v11', feature_list)
            v11_frame = pd.merge(v11_frame, ret_frame[['cate', 'user_id'] + feature_list], how='left')
            del action_type_frame
            del user_sku_group
            del ret_frame
        assert (len(v11_frame) == len(y_frame))
        npz.save_npz(v11_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v11_frame
        pass

    def __feature12(x):
        features = []
        same_frame = x[x.cate == x.cate1]
        for hour in HOUR_WINDOWS:
            f = np.searchsorted(
                np.sort(x['_action_1_' + str(hour) + 'hour_count'] * (-1)),
                -same_frame['_action_1_' + str(hour) + 'hour_count'].tolist()[0]
            )
            features.append(f)
        return ";".join(map(str, features))

    def featureV12():
        __FEATURE_FILE_NAME = 'v12.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别的类别排序特征')

        v12_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')[:100]
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')[:100]
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        base_frame = pd.merge(base_frame[['user_id', 'cate']], base_frame.rename(columns={'cate': 'cate1'}), how='left',
                              on=['user_id'])
        columns = ['_action_1_' + str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
        base_frame = base_frame[['user_id', 'cate', 'cate1'] + columns]
        base_frame.fillna(0, downcast='infer', inplace=True)
        user_cate_group = base_frame.groupby(['user_id', 'cate'], as_index=False)
        ret_frame = user_cate_group.apply(__feature12).reset_index(name='_v12')
        feature_list = ['_other_cate_' + str(hour) + 'hour_rank' for hour in HOUR_WINDOWS]
        split_feature(ret_frame, '_v12', feature_list)
        ret_frame = ret_frame[['user_id', 'cate'] + feature_list]
        v12_frame = pd.merge(v12_frame, ret_frame, how='left')
        assert (len(y_frame) == len(v12_frame))
        # v12_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v12_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame
        del ret_frame

        pass

    def __feature13(x):
        features = []
        for hour in HOUR_WINDOWS:
            f = __div(x['_action_4_' + str(hour) + 'hour_count'], x['_action_1_' + str(hour) + 'hour_count'])
            features.append(f)
            pass
        for hour in HOUR_WINDOWS:
            f = __div(x['_action_4_' + str(hour) + 'hour_count'], x['_action_2_' + str(hour) + 'hour_count'])
            features.append(f)
        for hour in HOUR_WINDOWS:
            f = __div(x['_action_4_' + str(hour) + 'hour_count'], x['_action_5_' + str(hour) + 'hour_count'])
            features.append(f)
        for hour in HOUR_WINDOWS:
            f = __div(x['_action_4_' + str(hour) + 'hour_count'], x['_action_6_' + str(hour) + 'hour_count'])
            features.append(f)
        return ";".join(map(str, features))

    def featureV13():
        __FEATURE_FILE_NAME = 'v13.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v13_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')

        base_frame.fillna(0, downcast='infer', inplace=True)
        base_frame['_v13'] = base_frame.apply(__feature13, axis=1)
        feature_list = ['_action_' + str(hour) + 'hour_buy_view_rate' for hour in HOUR_WINDOWS] + \
                       ['_action_' + str(hour) + 'hour_buy_atc_rate' for hour in HOUR_WINDOWS] + \
                       ['_action_' + str(hour) + 'hour_buy_follow_rate' for hour in HOUR_WINDOWS] + \
                       ['_action_' + str(hour) + 'hour_buy_click_rate' for hour in HOUR_WINDOWS]
        split_feature(base_frame, '_v13', feature_list)
        v13_frame = pd.merge(v13_frame, base_frame[['user_id', 'cate'] + feature_list], how='left')
        assert (len(y_frame) == len(v13_frame))
        # v13_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v13_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def __feature14(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV14():
        __FEATURE_FILE_NAME = 'v14.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])[
                           -100:]
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v14_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v14_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['cate', 'sku_id', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v14': __feature14})
            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_sku_action_' + str(type) + '_' + str(x) + '_max' for x in feature_list]
            split_feature(ret_frame, '_v14', feature_list)
            ret_frame_max = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.max)
            v14_frame = pd.merge(v14_frame, ret_frame_max[['cate', 'user_id'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_sku_action_' + str(type) + '_' + str(x) + '_mean' for x in feature_list]
            split_feature(ret_frame, '_v14', feature_list)
            ret_frame_mean = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.mean)
            v14_frame = pd.merge(v14_frame, ret_frame_mean[['cate', 'user_id'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_sku_action_' + str(type) + '_' + str(x) + '_min' for x in feature_list]
            split_feature(ret_frame, '_v14', feature_list)
            ret_frame_min = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.min)
            v14_frame = pd.merge(v14_frame, ret_frame_min[['cate', 'user_id'] + feature_list], how='left')

            del action_type_frame
            del user_sku_group
            del ret_frame
            del ret_frame_max
        assert (len(v14_frame) == len(y_frame))
        # v14_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v14_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v14_frame
        pass

    def featureV15():
        __FEATURE_FILE_NAME = 'v15.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'sku_id', 'brand', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range('2016-02-01', '2016-04-15')
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'sku_id', 'brand', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v15_frame = y_frame[['cate', 'user_id']]
        ret_frame = action_frame.groupby(['cate'], as_index=False)['time'].agg({'_all_time_cate_count': len})
        v15_frame = pd.merge(v15_frame, ret_frame, how='left')

        npz.save_npz(v15_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        assert (len(v15_frame) == len(y_frame))
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del ret_frame
        del v15_frame
        pass

    def __feature16(x):
        features = []

        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        mi = target_stamp
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                mi = x_sort[j]
                j -= 1
            features.append(target_stamp - mi)

        return ";".join(map(str, features))

    def featureV16():
        __FEATURE_FILE_NAME = 'v16.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v16_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v16_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_sku_group = action_type_frame.groupby(['cate', 'user_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_v16': __feature16})
            feature_list = [str(hour) + 'hour_min' for hour in HOUR_WINDOWS]
            feature_list = ['_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v16', feature_list)
            v16_frame = pd.merge(v16_frame, ret_frame[['cate', 'user_id'] + feature_list], how='left')
            del action_type_frame
            del user_sku_group
            del ret_frame
        assert (len(v16_frame) == len(y_frame))
        # v16_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v16_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        print(v16_frame)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v16_frame
        pass

    def __feature17(x):
        features = []

        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        mi = target_stamp
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                mi = x_sort[j]
                j -= 1
            features.append(target_stamp - mi)

        return ";".join(map(str, features))

    def featureV17():
        __FEATURE_FILE_NAME = 'v17.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))
        v17_frame = y_frame[['cate', 'user_id']]

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            sku_group = action_type_frame.groupby(['cate'], as_index=False)
            ret_frame = sku_group['time'].agg({'_v17': __feature17})
            feature_list = [str(hour) + 'hour_min' for hour in HOUR_WINDOWS]
            feature_list = ['_cate_action_' + str(type) + '_' + str(x) for x in feature_list]
            split_feature(ret_frame, '_v17', feature_list)
            v17_frame = pd.merge(v17_frame, ret_frame[['cate'] + feature_list], how='left')
            del ret_frame
            del action_type_frame
            del sku_group
        # v17_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v17_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        assert (len(v17_frame) == len(y_frame))
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v17_frame
        pass

    def featureV18():
        __FEATURE_FILE_NAME = 'v18.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])[
                           -100:]
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v18_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v18_frame, action_frame, how='left')
        for day in [1, 2, 3, 7, 15, 30, 60]:
            day_action_frame = action_frame[action_frame.time >= util.move_day(date, -day)]
            v18_frame = pd.merge(v18_frame, day_action_frame.groupby(
                ['user_id', 'cate'], as_index=False)['sku_id'].agg(
                {'_' + str(day) + 'day_visit_items_in_cate': np.size}), how='left')
            v18_frame = pd.merge(v18_frame, day_action_frame.groupby(
                ['user_id'], as_index=False)['sku_id'].agg(
                {'_' + str(day) + 'day_visit_items_in_user': np.size}), how='left')
            v18_frame = pd.merge(v18_frame, day_action_frame.groupby(
                ['user_id'], as_index=False)['cate'].agg(
                {'_' + str(day) + 'day_visit_cate_in_user': np.size}), how='left')

            comment_frame = pd.read_csv('../data/JData_Comment.csv')
            comment_frame = comment_frame[comment_frame.dt < date]
            comment_frame.sort_values(by='dt', inplace=True)
            comment_frame = comment_frame.groupby(['sku_id'], as_index=False)[
                'comment_num', 'has_bad_comment', 'bad_comment_rate'].agg(lambda x: x.as_matrix()[-1])
            comment_frame = pd.merge(day_action_frame[['sku_id', 'cate', 'user_id']].drop_duplicates(), comment_frame,
                                     how='left')

            v18_frame = pd.merge(v18_frame,
                                 comment_frame.groupby(['user_id', 'cate'], as_index=False)['comment_num'].agg(
                                     {'_' + str(day) + 'day_comment_num_min': np.min,
                                      '_' + str(day) + 'day_comment_num_avg': np.mean,
                                      '_' + str(day) + 'day_comment_num_max': np.max}), how='left')
            v18_frame = pd.merge(v18_frame,
                                 comment_frame.groupby(['user_id', 'cate'], as_index=False)['has_bad_comment'].agg(
                                     {'_' + str(day) + 'day_has_bad_comment_min': np.min,
                                      '_' + str(day) + 'day_has_bad_comment_avg': np.mean,
                                      '_' + str(day) + 'day_has_bad_comment_max': np.max}), how='left')
            v18_frame = pd.merge(v18_frame,
                                 comment_frame.groupby(['user_id', 'cate'], as_index=False)['bad_comment_rate'].agg(
                                     {'_' + str(day) + 'day_bad_comment_rate_min': np.min,
                                      '_' + str(day) + 'day_bad_comment_rate_avg': np.mean,
                                      '_' + str(day) + 'day_bad_comment_rate_max': np.max}), how='left')
        assert (len(v18_frame) == len(y_frame))
        # v18_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v18_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v18_frame
        del comment_frame
        pass

    def __feature19(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_DAY = 60 * 60 * 24
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for day in DAY_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_DAY * day:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV19():
        __FEATURE_FILE_NAME = 'v19.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v19_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v19_frame, action_frame, how='left')
        action_frame['hour'] = action_frame['time'].apply(lambda x: __hour_segment(x))
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for hour_seg in range(3):
            for type in range(1, 7):
                action_type_frame = action_frame[(action_frame.type == type) & (action_frame.hour == hour_seg)]
                sku_group = action_type_frame.groupby(['cate'], as_index=False)
                ret_frame = sku_group['time'].agg({'_v19': __feature19})
                feature_list = ['seg_max', 'seg_min', 'seg_offset'] + [
                    str(day) + 'day_' + str(hour_seg) + 'hour_seg_count' for day in DAY_WINDOWS]
                feature_list = ['_cate_action_' + str(type) + '_' + str(x) for x in feature_list]
                split_feature(ret_frame, '_v19', feature_list)
                v19_frame = pd.merge(v19_frame, ret_frame[['cate'] + feature_list], how='left')
                del ret_frame
                del action_type_frame
                del sku_group
        # v19_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v19_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        assert (len(v19_frame) == len(y_frame))
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v19_frame
        pass

    def __feature20(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_DAY = 60 * 60 * 24
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for day in DAY_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_DAY * day:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV20():
        __FEATURE_FILE_NAME = 'v20.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v20_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v20_frame, action_frame, how='left')
        action_frame['hour'] = action_frame['time'].apply(lambda x: __hour_segment(x))
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for hour_seg in range(3):
            for type in range(1, 7):
                action_type_frame = action_frame[(action_frame.type == type) & (action_frame.hour == hour_seg)]
                user_group = action_type_frame.groupby(['user_id'], as_index=False)
                ret_frame = user_group['time'].agg({'_v20': __feature20})
                feature_list = ['seg_max', 'seg_min', 'seg_offset'] + [
                    str(day) + 'day_' + str(hour_seg) + 'hour_seg_count' for day in DAY_WINDOWS]
                feature_list = ['_user_action_' + str(type) + '_' + str(x) for x in feature_list]
                split_feature(ret_frame, '_v20', feature_list)
                v20_frame = pd.merge(v20_frame, ret_frame[['user_id'] + feature_list], how='left')
                del action_type_frame
                del user_group
                del ret_frame
        assert (len(y_frame) == len(v20_frame))
        # v20_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v20_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v20_frame
        pass

    def featureV21():
        pass

    def __feature22(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_DAY = 60 * 60 * 24
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for day in DAY_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_DAY * day:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV22():
        __FEATURE_FILE_NAME = 'v22.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v22_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v22_frame, action_frame, how='left')
        action_frame['hour'] = action_frame['time'].apply(lambda x: __hour_segment(x))
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for hour_seg in range(3):
            for type in range(1, 7):
                action_type_frame = action_frame[(action_frame.type == type) & (action_frame.hour == hour_seg)]
                user_sku_group = action_type_frame.groupby(['cate', 'user_id'], as_index=False)
                ret_frame = user_sku_group['time'].agg({'_v22': __feature22})
                feature_list = ['seg_max', 'seg_min', 'seg_offset'] + [
                    str(day) + 'day_' + str(hour_seg) + 'hour_seg_count' for day in DAY_WINDOWS]
                feature_list = ['_action_' + str(type) + '_' + str(x) for x in feature_list]
                split_feature(ret_frame, '_v22', feature_list)
                v22_frame = pd.merge(v22_frame, ret_frame[['cate', 'user_id'] + feature_list], how='left')
                del action_type_frame
                del user_sku_group
            del ret_frame
        assert (len(v22_frame) == len(y_frame))
        # v22_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v22_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v22_frame
        pass

    def featureV23():
        __FEATURE_FILE_NAME = 'v23.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -2), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v23_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v23_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))
        action_frame.sort_values(by='time', inplace=True)
        sequences = np.zeros(len(action_frame))
        time_map = {}
        SECONDS_PER_HOUR = 60 * 60
        i = 0
        for row in action_frame.itertuples():
            if row[2] not in time_map:
                seq = 0
                last = -100000000
            else:
                seq, last = time_map[row[2]]
            if row[4] - last >= SECONDS_PER_HOUR:
                seq += 1
            sequences[i] = seq
            last = row[4]
            i += 1
            time_map[row[2]] = (seq, last)
        action_frame['sequence'] = sequences

        v23_frame = y_frame[['user_id', 'cate']]

        for hour in HOUR_WINDOWS:
            action_hour_frame = action_frame[action_frame.time >= target_stamp - SECONDS_PER_HOUR * hour]
            action_cate_group = action_hour_frame.groupby(['user_id', 'cate'], as_index=False)
            action_user_cate_frame = action_cate_group['sequence'].agg(
                {'_action_' + str(hour) + 'hour_seq_count': lambda x: len(np.unique(x))})
            action_user_group = action_hour_frame.groupby(['user_id'], as_index=False)
            action_user_frame = action_user_group['sequence'].agg(
                {'_user_action_' + str(hour) + 'hour_seq_count': lambda x: len(np.unique(x))})
            v23_frame = pd.merge(v23_frame, action_user_cate_frame, how='left')
            v23_frame = pd.merge(v23_frame, action_user_frame, how='left')
            v23_frame['_user_action_' + str(hour) + 'hour_visit_cate_rate'] = v23_frame.apply(
                lambda x: None if x['_user_action_' + str(hour) + 'hour_seq_count'] == 0 or
                                  np.isnan(x['_user_action_' + str(hour) + 'hour_seq_count']) \
                    else x['_action_' + str(hour) + 'hour_seq_count'] /
                         x['_user_action_' + str(hour) + 'hour_seq_count']
                , axis=1)

        assert (len(v23_frame) == len(y_frame))
        # v23_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v23_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v23_frame
        pass

    def featureV24():
        pass

    def featureV25():
        __FEATURE_FILE_NAME = 'v25.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v25_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v2.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v2.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v2.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v2.npz')
        base_frame.fillna(0, downcast='infer', inplace=True)

        for hour in HOUR_WINDOWS:
            base_frame['_cate_action_' + str(hour) + 'hour_weighted_count'] = \
                0.1 * base_frame['_cate_action_1_' + str(hour) + 'hour_count'] + \
                0.7 * base_frame['_cate_action_2_' + str(hour) + 'hour_count'] + \
                -0.7 * base_frame['_cate_action_3_' + str(hour) + 'hour_count'] + \
                1 * base_frame['_cate_action_4_' + str(hour) + 'hour_count'] + \
                0.5 * base_frame['_cate_action_5_' + str(hour) + 'hour_count'] + \
                0.2 * base_frame['_cate_action_6_' + str(hour) + 'hour_count']
        v25_frame = pd.merge(v25_frame,
                             base_frame[
                                 ['_cate_action_' + str(hour) + 'hour_weighted_count' for hour in HOUR_WINDOWS] + \
                                 ['user_id', 'cate']], how='left')

        assert (len(y_frame) == len(v25_frame))
        # v25_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v25_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def featureV26():
        __FEATURE_FILE_NAME = 'v26.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v26_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v3.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v3.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v3.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v3.npz')
        base_frame.fillna(0, downcast='infer', inplace=True)

        for hour in HOUR_WINDOWS:
            base_frame['_user_action_' + str(hour) + 'hour_weighted_count'] = \
                0.1 * base_frame['_user_action_1_' + str(hour) + 'hour_count'] + \
                0.7 * base_frame['_user_action_2_' + str(hour) + 'hour_count'] + \
                -0.7 * base_frame['_user_action_3_' + str(hour) + 'hour_count'] + \
                1 * base_frame['_user_action_4_' + str(hour) + 'hour_count'] + \
                0.5 * base_frame['_user_action_5_' + str(hour) + 'hour_count'] + \
                0.2 * base_frame['_user_action_6_' + str(hour) + 'hour_count']
        v26_frame = pd.merge(v26_frame,
                             base_frame[
                                 ['_user_action_' + str(hour) + 'hour_weighted_count' for hour in HOUR_WINDOWS] + \
                                 ['user_id', 'cate']], how='left')

        assert (len(y_frame) == len(v26_frame))
        # v26_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v26_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def featureV27():
        __FEATURE_FILE_NAME = 'v27.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v27_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        base_frame.fillna(0, downcast='infer', inplace=True)

        for hour in HOUR_WINDOWS:
            base_frame['_action_' + str(hour) + 'hour_weighted_count'] = \
                0.1 * base_frame['_action_1_' + str(hour) + 'hour_count'] + \
                0.7 * base_frame['_action_2_' + str(hour) + 'hour_count'] + \
                -0.7 * base_frame['_action_3_' + str(hour) + 'hour_count'] + \
                1 * base_frame['_action_4_' + str(hour) + 'hour_count'] + \
                0.5 * base_frame['_action_5_' + str(hour) + 'hour_count'] + \
                0.2 * base_frame['_action_6_' + str(hour) + 'hour_count']
        v27_frame = pd.merge(v27_frame,
                             base_frame[
                                 ['_action_' + str(hour) + 'hour_weighted_count' for hour in HOUR_WINDOWS] + \
                                 ['user_id', 'cate']], how='left')

        assert (len(y_frame) == len(v27_frame))
        # v27_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v27_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def __feature28(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV28():
        __FEATURE_FILE_NAME = 'v28.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v28_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v28_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取所有操作的统计特征')
        user_sku_group = action_frame.groupby(['user_id'], as_index=False)
        ret_frame = user_sku_group['time'].agg({'_v28': __feature28})
        feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
        feature_list = ['_all_user_action_' + str(x) for x in feature_list]
        split_feature(ret_frame, '_v28', feature_list)
        v28_frame = pd.merge(v28_frame, ret_frame[['user_id'] + feature_list], how='left')
        del action_frame
        del user_sku_group
        del ret_frame
        assert (len(v28_frame) == len(y_frame))
        # v28_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v28_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v28_frame
        pass

    def __feature29(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))

        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV29():
        __FEATURE_FILE_NAME = 'v29.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])[
                           -10000:]
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'sku_id', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v29_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v29_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            sku_group = action_type_frame.groupby(['cate', 'sku_id'], as_index=False)
            ret_frame = sku_group['time'].agg({'_v29': __feature29})
            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_only_sku_action_' + str(type) + '_' + str(x) + '_max' for x in feature_list]
            split_feature(ret_frame, '_v29', feature_list)
            ret_frame_max = ret_frame.groupby(['cate'], as_index=False)[feature_list].agg(np.max)
            v29_frame = pd.merge(v29_frame, ret_frame_max[['cate'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_only_sku_action_' + str(type) + '_' + str(x) + '_mean' for x in feature_list]
            split_feature(ret_frame, '_v29', feature_list)
            ret_frame_mean = ret_frame.groupby(['cate'], as_index=False)[feature_list].agg(np.mean)
            v29_frame = pd.merge(v29_frame, ret_frame_mean[['cate'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_only_sku_action_' + str(type) + '_' + str(x) + '_min' for x in feature_list]
            split_feature(ret_frame, '_v29', feature_list)
            ret_frame_min = ret_frame.groupby(['cate'], as_index=False)[feature_list].agg(np.min)
            v29_frame = pd.merge(v29_frame, ret_frame_min[['cate'] + feature_list], how='left')

            del action_type_frame
            del sku_group
            del ret_frame
            del ret_frame_max
        assert (len(v29_frame) == len(y_frame))
        # v29_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v29_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v29_frame
        pass

    def __feature30(x):
        features = []
        y = x.sort_values(by=['time', 'type'], ascending=[True, True])

        in_mart = 0
        buy = 0

        in_mart_and_buy = 0

        for index, item in y.iterrows():
            if item['type'] == 2:
                in_mart = 1
            elif item['type'] == 3:
                in_mart = 0
            elif item['type'] == 4:
                buy = 1
                if in_mart:
                    in_mart_and_buy = 1

        # 是否还在购物车里面并且还没买, _is_in_mart_not_buy
        features.append(1 if in_mart and not buy else 0)

        # 是否还在购物车里面并且买了, _is_in_mart_buy
        features.append(in_mart_and_buy)

        return ";".join(map(str, features))

    def __feature30_1(x):
        features = []

        # 是否还在购物车并且没买
        features.append(x['_is_in_mart_not_buy'].min())

        # 是否还在购物车并且买了
        features.append(x['_is_in_mart_and_buy'].max())

        return ";".join(map(str, features))

        pass

    def __feature30_2(x):
        features = []

        x_same = x[x.cate == x.cate1]
        x_other = x[x.cate != x.cate1]

        if x_same['_is_in_mart_not_buy_x'].max() == 1 and x_other['_is_in_mart_and_buy_y'].max() == 1:
            features.append(1)
        else:
            features.append(0)

        return ";".join(map(str, features))

    def featureV30():
        __FEATURE_FILE_NAME = 'v30.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取自定义的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['sku_id', 'cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['sku_id', 'cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        action_frame = action_frame[(action_frame.type != 1) & (action_frame.type != 6)]
        v30_frame = y_frame[['cate', 'user_id', 'y']]
        action_frame = pd.merge(v30_frame, action_frame, how='left')
        if __DEBUG:
            action_frame['time'].fillna(-1, downcast='infer', inplace=True)
            action_frame = action_frame[action_frame.time != -1]
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        user_sku_group = action_frame.groupby(['user_id', 'cate', 'sku_id'], as_index=False)
        ret_frame = user_sku_group.apply(__feature30).reset_index(name='_v30')
        feature_list = ['_is_in_mart_not_buy', '_is_in_mart_and_buy']
        split_feature(ret_frame, '_v30', feature_list)
        print('Done')
        user_cate_group = ret_frame.groupby(['user_id', 'cate'], as_index=False)
        ret_frame = user_cate_group.apply(__feature30_1).reset_index(name='_v30')
        split_feature(ret_frame, '_v30', feature_list)
        v30_frame = pd.merge(v30_frame, ret_frame[['user_id', 'cate'] + feature_list], how='left')
        assert (len(v30_frame) == len(y_frame))

        cross_cate_frame = pd.merge(ret_frame,
                                    ret_frame.rename(columns={'cate': 'cate1'})[['user_id', 'cate1'] + feature_list],
                                    how='left', on=['user_id'])

        user_group = cross_cate_frame.groupby(['user_id', 'cate'], as_index=False)
        ret_frame = user_group.apply(__feature30_2).reset_index(name='_v30')
        feature_list = ['_is_in_cart_and_buy_other_cate']
        split_feature(ret_frame, '_v30', feature_list)
        v30_frame = pd.merge(v30_frame, ret_frame[['user_id', 'cate'] + feature_list], how='left')
        assert (len(v30_frame) == len(y_frame))
        # v30_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v30_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v30_frame
        pass

    def __featureV31(x):
        features = []
        for hour in HOUR_WINDOWS:
            total = 0
            for type in range(1, 7):
                total += x['_action_' + str(type) + '_' + str(hour) + 'hour_count']
            for type in range(1, 7):
                features.append(
                    -1 if total == 0 else x['_action_' + str(type) + '_' + str(hour) + 'hour_count'] / total)

        return ";".join(map(str, features))

    def featureV31():
        __FEATURE_FILE_NAME = 'v31.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v31_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v1.npz')
        base_frame.fillna(0, downcast='infer', inplace=True)

        feature_list = []

        for hour in HOUR_WINDOWS:
            for type in range(1, 7):
                feature_list.append('_action_' + str(type) + '_' + str(hour) + 'hour_rate')
        base_frame['_v31'] = base_frame.apply(__featureV31, axis=1)
        split_feature(base_frame, '_v31', feature_list)

        v31_frame = pd.merge(v31_frame, base_frame[['user_id', 'cate'] + feature_list], how='left')
        assert (len(y_frame) == len(v31_frame))
        # v31_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v31_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    def __feature32(x):
        features = []

        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV32():
        __FEATURE_FILE_NAME = 'v32.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'sku_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v32_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v32_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        product_frame = pd.read_csv('../data/JData_Product.csv')
        action_frame = pd.merge(action_frame, product_frame, how='left')

        logger.info('提取每种操作的统计特征')
        for value in [-1, 1, 2, 3]:
            for type in range(1, 7):
                action_type_frame = action_frame[
                    (action_frame.type == type) & (action_frame.cate == 8) & (action_frame.a1 == value)]
                user_sku_group = action_type_frame.groupby(['user_id', 'cate'], as_index=False)
                ret_frame = user_sku_group['time'].agg({'_v32': __feature32})
                feature_list = ['_action_a1_' + str(value) + '_' + str(type) + '_' + str(hour) + 'hour_count' for hour
                                in HOUR_WINDOWS]
                split_feature(ret_frame, '_v32', feature_list)
                v32_frame = pd.merge(v32_frame, ret_frame[['user_id', 'cate'] + feature_list], how='left')
                assert (len(v32_frame) == len(y_frame))
                del action_type_frame
                del user_sku_group
                del ret_frame
                action_type_frame = action_frame[
                    (action_frame.type == type) & (action_frame.cate == 8) & (action_frame.a2 == value)]
                user_sku_group = action_type_frame.groupby(['user_id', 'cate'], as_index=False)
                ret_frame = user_sku_group['time'].agg({'_v32': __feature32})
                feature_list = ['_action_a2_' + str(value) + '_' + str(type) + '_' + str(hour) + 'hour_count' for hour
                                in HOUR_WINDOWS]
                split_feature(ret_frame, '_v32', feature_list)
                v32_frame = pd.merge(v32_frame, ret_frame[['user_id', 'cate'] + feature_list], how='left')
                assert (len(v32_frame) == len(y_frame))
                del action_type_frame
                del user_sku_group
                del ret_frame
                action_type_frame = action_frame[
                    (action_frame.type == type) & (action_frame.cate == 8) & (action_frame.a3 == value)]
                user_sku_group = action_type_frame.groupby(['user_id', 'cate'], as_index=False)
                ret_frame = user_sku_group['time'].agg({'_v32': __feature32})
                feature_list = ['_action_a3_' + str(value) + '_' + str(type) + '_' + str(hour) + 'hour_count' for hour
                                in
                                HOUR_WINDOWS]
                split_feature(ret_frame, '_v32', feature_list)
                v32_frame = pd.merge(v32_frame, ret_frame[['user_id', 'cate'] + feature_list], how='left')
                assert (len(v32_frame) == len(y_frame))
                del action_type_frame
                del user_sku_group
                del ret_frame
        assert (len(v32_frame) == len(y_frame))
        # v1_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v32_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v32_frame
        pass

    def __feature33(x):
        features = []

        features.append(int(target_stamp - np.max(x)))
        features.append(int(target_stamp - np.min(x)))
        features.append(int(np.max(x) - np.min(x)))
        SECONDS_PER_HOUR = 60 * 60
        x_sort = sorted(x)
        j = len(x_sort) - 1
        count = 0
        for hour in HOUR_WINDOWS:
            while j >= 0 and x_sort[j] >= target_stamp - SECONDS_PER_HOUR * hour:
                count += 1
                j -= 1
            features.append(count)

        return ";".join(map(str, features))

    def featureV33():
        __FEATURE_FILE_NAME = 'v33.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v33_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v33_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取所有操作的统计特征')
        user_sku_group = action_frame.groupby(['user_id'], as_index=False)
        ret_frame = user_sku_group['time'].agg({'_v33': __feature33})
        feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
        feature_list = ['_all_cate_action_' + str(x) for x in feature_list]
        split_feature(ret_frame, '_v33', feature_list)
        v33_frame = pd.merge(v33_frame, ret_frame[['user_id'] + feature_list], how='left')
        del action_frame
        del user_sku_group
        del ret_frame
        assert (len(v33_frame) == len(y_frame))
        # v33_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v33_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v33_frame
        pass

    def featureV34():
        __FEATURE_FILE_NAME = 'v34.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'user_id', 'sku_id', 'type', 'time', 'model_id'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'user_id', 'sku_id', 'type', 'time', 'model_id'])

        # action_frame = action_frame.drop_duplicates()
        action_frame = action_frame[action_frame.type == 6]

        v34_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v34_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取所有操作的统计特征')
        SECONDS_PER_HOUR = 60 * 60
        for hour in HOUR_WINDOWS:
            action_hour_frame = action_frame[action_frame.time >= target_stamp - SECONDS_PER_HOUR * hour]
            user_sku_group = action_hour_frame.groupby(['user_id', 'sku_id', 'cate', 'model_id'], as_index=False)
            ret_frame = user_sku_group['time'].agg({'_count': len})
            ret_frame = ret_frame.groupby(['user_id', 'cate', 'model_id'], as_index=False)['_count'].agg(np.max)
            for model_id in [0, 216, 217]:
                ret_frame_model = ret_frame[ret_frame.model_id == model_id]
                ret_frame_model['_action_model_' + str(model_id) + '_' + str(hour) + 'hour_count'] = ret_frame_model[
                    '_count']
                v34_frame = pd.merge(v34_frame, ret_frame_model[
                    ['user_id', 'cate', '_action_model_' + str(model_id) + '_' + str(hour) + 'hour_count']], how='left')
                del ret_frame_model
            del user_sku_group
            del ret_frame
            del action_hour_frame

        del action_frame
        assert (len(v34_frame) == len(y_frame))
        # v34_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v34_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del v34_frame
        pass

    def featureV35():
        __FEATURE_FILE_NAME = 'v35.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品-类别行为的统计特征')
        if __DEBUG:
            action_dates = util.date_range(util.move_day(date, -1), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates,
                                               columns=['cate', 'brand', 'user_id', 'type', 'time'])
        else:
            action_dates = util.date_range(util.move_day(date, -60), util.move_day(date, -1))
            action_frame = read_action_by_date(action_dates, columns=['cate', 'brand', 'user_id', 'type', 'time'])

        # action_frame = action_frame.drop_duplicates()
        v35_frame = y_frame[['cate', 'user_id']]
        action_frame = pd.merge(v35_frame, action_frame, how='left')
        action_frame['time'] = action_frame['time'].apply(
            lambda x: __time2stamp(x))

        logger.info('提取每种操作的统计特征')
        for type in range(1, 7):
            action_type_frame = action_frame[action_frame.type == type]
            user_brand_group = action_type_frame.groupby(['cate', 'brand', 'user_id'], as_index=False)
            ret_frame = user_brand_group['time'].agg({'_v35': __feature14})
            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_brand_action_' + str(type) + '_' + str(x) + '_max' for x in feature_list]
            split_feature(ret_frame, '_v35', feature_list)
            ret_frame_max = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.max)
            v35_frame = pd.merge(v35_frame, ret_frame_max[['cate', 'user_id'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_brand_action_' + str(type) + '_' + str(x) + '_mean' for x in feature_list]
            split_feature(ret_frame, '_v35', feature_list)
            ret_frame_mean = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.mean)
            v35_frame = pd.merge(v35_frame, ret_frame_mean[['cate', 'user_id'] + feature_list], how='left')

            feature_list = ['max', 'min', 'offset'] + [str(hour) + 'hour_count' for hour in HOUR_WINDOWS]
            feature_list = ['_brand_action_' + str(type) + '_' + str(x) + '_min' for x in feature_list]
            split_feature(ret_frame, '_v35', feature_list)
            ret_frame_min = ret_frame.groupby(['user_id', 'cate'], as_index=False)[feature_list].agg(np.min)
            v35_frame = pd.merge(v35_frame, ret_frame_min[['cate', 'user_id'] + feature_list], how='left')

            del action_type_frame
            del user_brand_group
            del ret_frame
            del ret_frame_max
        assert (len(v35_frame) == len(y_frame))
        # v14_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v35_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del action_frame
        del v35_frame
        pass

    def __feature36(x):
        features = []
        for hour in HOUR_WINDOWS:
            f = __div(x['_user_action_4_' + str(hour) + 'hour_count'], x['_user_action_1_' + str(hour) + 'hour_count'])
            features.append(f)
            pass
        for hour in HOUR_WINDOWS:
            f = __div(x['_user_action_4_' + str(hour) + 'hour_count'], x['_user_action_2_' + str(hour) + 'hour_count'])
            features.append(f)
        for hour in HOUR_WINDOWS:
            f = __div(x['_user_action_4_' + str(hour) + 'hour_count'], x['_user_action_5_' + str(hour) + 'hour_count'])
            features.append(f)
        for hour in HOUR_WINDOWS:
            f = __div(x['_user_action_4_' + str(hour) + 'hour_count'], x['_user_action_6_' + str(hour) + 'hour_count'])
            features.append(f)
        return ";".join(map(str, features))

    def featureV36():
        __FEATURE_FILE_NAME = 'v36.npz'
        logger.info('提取特征: ' + __FEATURE_FILE_NAME)
        logger.info('提取用户-商品行为的统计特征')

        v36_frame = y_frame[['user_id', 'cate']]
        if __DEBUG:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v3.npz')
        else:
            # base_frame = pd.read_csv('../feature_pool/' + __DATASET + '/v1.csv')
            base_frame = npz.load_npz('../feature_pool/' + __DATASET + '/v3.npz')

        base_frame.fillna(0, downcast='infer', inplace=True)
        base_frame['_v36'] = base_frame.apply(__feature36, axis=1)
        feature_list = ['_user_action_' + str(hour) + 'hour_buy_view_rate' for hour in HOUR_WINDOWS] + \
                       ['_user_action_' + str(hour) + 'hour_buy_atc_rate' for hour in HOUR_WINDOWS] + \
                       ['_user_action_' + str(hour) + 'hour_buy_follow_rate' for hour in HOUR_WINDOWS] + \
                       ['_user_action_' + str(hour) + 'hour_buy_click_rate' for hour in HOUR_WINDOWS]
        split_feature(base_frame, '_v36', feature_list)
        v36_frame = pd.merge(v36_frame, base_frame[['user_id', 'cate'] + feature_list], how='left')
        assert (len(y_frame) == len(v36_frame))
        # v13_frame.to_csv('../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME, index=False)
        npz.save_npz(v36_frame, '../feature_pool/' + __DATASET + '/' + __FEATURE_FILE_NAME)
        logger.info('提取特征完成: ' + __FEATURE_FILE_NAME)
        del base_frame

        pass

    f_map = {
        1: featureV1,
        2: featureV2,
        3: featureV3,
        4: featureV4,
        5: featureV5,
        6: featureV6,
        7: featureV7,
        8: featureV8,
        9: featureV9,
        10: featureV10,
        11: featureV11,
        12: featureV12,
        13: featureV13,
        14: featureV14,
        15: featureV15,
        16: featureV16,
        17: featureV17,
        18: featureV18,
        19: featureV19,
        20: featureV20,
        21: featureV21,
        22: featureV22,
        23: featureV23,
        24: featureV24,
        25: featureV25,
        26: featureV26,
        27: featureV27,
        28: featureV28,
        29: featureV29,
        30: featureV30,
        31: featureV31,
        32: featureV32,
        33: featureV33,
        34: featureV34,
        35: featureV35,
        36: featureV36,
    }

    if f is not None:
        f_map[int(f)]()
    pass


if __name__ == "__main__":
    logger = logging.getLogger('gen_wxf')
    logger.setLevel(logging.DEBUG)

    date = sys.argv[1]
    offset = int(sys.argv[2])
    __DATASET = sys.argv[3]  # 样本集
    __DEBUG = False if len(sys.argv) > 4 and int(sys.argv[4]) == 0 else True
    init()

    # 初始化日志信息

    # featureY(date, offset)
    # featureY1(date, offset)
    extract_featrure(date, sys.argv[5] if len(sys.argv) > 5 else None)
